﻿namespace DSR.Web;

public static class Settings
{
    public static string ApiRoot = "http://localhost:5212/api";

    public static string IdentityRoot = "http://localhost:5114";
    public static string ClientId = "frontend";
    public static string ClientSecret = "secret";
}
