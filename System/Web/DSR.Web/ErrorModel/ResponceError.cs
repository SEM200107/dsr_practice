﻿namespace DSR.Web.ErrorModel
{
    public class ResponceError
    {
        public int? ErrorCode { get; set; }
        public string? Message { get; set; }
    }
}
