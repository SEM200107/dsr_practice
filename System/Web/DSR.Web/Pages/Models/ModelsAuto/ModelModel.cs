﻿using FluentValidation;

namespace DSR.Web.Pages.Models.ModelsAuto
{
    public class ModelModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
    public class ModelModelValidator : AbstractValidator<ModelModel>
    {
        public ModelModelValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");
        }
        public Func<object, string, Task<IEnumerable<string>>> ValidateValue => async (model, propertyName) =>
        {
            var result = await ValidateAsync(ValidationContext<ModelModel>.CreateWithOptions((ModelModel)model, x => x.IncludeProperties(propertyName)));
            if (result.IsValid)
                return Array.Empty<string>();
            return result.Errors.Select(e => e.ErrorMessage);
        };
    }
}
