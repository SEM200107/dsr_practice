﻿namespace DSR.Web.Pages.Models.ModelsAuto
{
    public class ModelListItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
