﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Models.ModelsAuto;
using System.Text;
using System.Text.Json;

namespace DSR.Web.Pages.Models.Services
{
    public class ModelService : IModelService
    {
        private readonly HttpClient _httpClient;

        public ModelService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<ModelListItem>> GetModels()
        {
            string url = $"{Settings.ApiRoot}/v1/models";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<IEnumerable<ModelListItem>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new List<ModelListItem>();

            return data;
        }

        public async Task<ModelListItem> GetModel(int id)
        {
            string url = $"{Settings.ApiRoot}/v1/models/{id}";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<ModelListItem>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new ModelListItem();

            return data;
        }

        public async Task<ResponceError> AddModel(ModelModel model)
        {
            string url = $"{Settings.ApiRoot}/v1/models";

            var body = JsonSerializer.Serialize(model);
            var request = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(url, request);

            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }

        public async Task<ResponceError> UpdateModel(int id, ModelModel model)
        {
            string url = $"{Settings.ApiRoot}/v1/models/{id}";

            var body = JsonSerializer.Serialize(model);
            var request = new StringContent(body, Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(url, request);

            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }

        public async Task<ResponceError> DeleteModel(int id)
        {
            string url = $"{Settings.ApiRoot}/v1/models/{id}";

            var response = await _httpClient.DeleteAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }
    }
}
