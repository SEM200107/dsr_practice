﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Models.ModelsAuto;

namespace DSR.Web.Pages.Models.Services
{
    public interface IModelService
    {
        Task<IEnumerable<ModelListItem>> GetModels();
        Task<ModelListItem> GetModel(int id);
        Task<ResponceError> AddModel(ModelModel model);
        Task<ResponceError> UpdateModel(int id, ModelModel model);
        Task<ResponceError> DeleteModel(int id);
    }
}
