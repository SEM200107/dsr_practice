﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.ResetPassword.Models;

namespace DSR.Web.Pages.ResetPassword.Services
{
    public interface IResetPasswordService
    {
        Task<ResponceError> ResetPassword(ResetPasswordModel model);
    }
}
