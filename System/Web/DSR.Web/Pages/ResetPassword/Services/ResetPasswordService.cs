﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.ResetPassword.Models;
using System.Text;
using System.Text.Json;

namespace DSR.Web.Pages.ResetPassword.Services
{
    public class ResetPasswordService : IResetPasswordService
    {
        private readonly HttpClient _httpClient;

        public ResetPasswordService(HttpClient _httpClient)
        {
            this._httpClient = _httpClient;
        }
        public async Task<ResponceError> ResetPassword(ResetPasswordModel model)
        {
            string url = $"{Settings.ApiRoot}/v1/accounts/resetpassword";

            var body = JsonSerializer.Serialize(model);
            var request = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(url, request);
            
            var content = await response.Content.ReadAsStringAsync();

            var result = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                result = JsonSerializer.Deserialize<ResponceError>(content);
                return result;
            }
            return result;
        }
    }
}
