﻿namespace DSR.Web.Pages.Comments.Models
{
    public class CommentListItem
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string FullNameUser { get; set; }
        public int AutomobileId { get; set; }
    }    
}
