﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Comments.Models;

namespace DSR.Web.Pages.Comments.Services
{
    public interface ICommentService
    {
        Task<IEnumerable<CommentListItem>> GetComments(int idAuto);
        Task<CommentListItem> GetComment(int id);
        Task<ResponceError> AddComment(CommentModel model);
        Task<ResponceError> UpdateComment(int id, CommentModel model);
        Task<ResponceError> DeleteComment(int id);
    }
}
