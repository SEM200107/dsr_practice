﻿namespace DSR.Web.Pages.Automobiles.Models
{
    public class AutoListItem
    {
        public int Id { get; set; }
        public string Number { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Color { get; set; } = string.Empty;
        public int ColorId { get; set; }
        public string Model { get; set; } = string.Empty;
        public int ModelId { get; set; }
        public string Mark { get; set; } = string.Empty;
        public int MarkId { get; set; }
    }
}
