﻿namespace DSR.Web.Pages.Automobiles.Models
{
    public class CharacteristicModel
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
    }
}
