﻿using FluentValidation;

namespace DSR.Web.Pages.Automobiles.Models
{
    public class AutoModel
    {
        public int? Id { get; set; }
        public string Number { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public int ColorId { get; set; }
        public int MarkId { get; set; }
    }
    public class AutoModelValidator : AbstractValidator<AutoModel>
    {
        public AutoModelValidator()
        {
            RuleFor(x => x.Number)
                .NotEmpty().WithMessage("Number is required")
                .MaximumLength(9).WithMessage("Number is long");
            RuleFor(x => x.Description)
                .NotEmpty().WithMessage("Description is required")
                .MaximumLength(1000).WithMessage("Description is long");
            RuleFor(x => x.ColorId)
                .GreaterThan(0).WithMessage("Please, select an color");
            RuleFor(x => x.MarkId)
                .GreaterThan(0).WithMessage("Please, select an mark");
        }
        public Func<object, string, Task<IEnumerable<string>>> ValidateValue => async (model, propertyName) =>
        {
            var result = await ValidateAsync(ValidationContext<AutoModel>.CreateWithOptions((AutoModel)model, x => x.IncludeProperties(propertyName)));
            if (result.IsValid)
                return Array.Empty<string>();
            return result.Errors.Select(e => e.ErrorMessage);
        };
    }
}
