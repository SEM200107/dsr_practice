﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Automobiles.Models;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace DSR.Web.Pages.Automobiles.Services
{
    public class AutoService : IAutoService
    {
        private readonly HttpClient _httpClient;

        public AutoService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<AutoListItem>> GetAutos(int offset = 0, int limit = 10)
        {
            string url = $"{Settings.ApiRoot}/v1/autos?offset={offset}&limit={limit}";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<IEnumerable<AutoListItem>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new List<AutoListItem>();

            return data;
        }

        public async Task<AutoListItem> GetAuto(int id)
        {
            string url = $"{Settings.ApiRoot}/v1/autos/{id}";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<AutoListItem>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new AutoListItem();

            return data;
        }

        public async Task<ResponceError> AddAuto(AutoModel model)
        {
            string url = $"{Settings.ApiRoot}/v1/autos";

            var body = JsonSerializer.Serialize(model);
            var request = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(url, request);

            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }

        public async Task<ResponceError> UpdateAuto(int id, AutoModel model)
        {
            string url = $"{Settings.ApiRoot}/v1/autos/{id}";

            var body = JsonSerializer.Serialize(model);
            var request = new StringContent(body, Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(url, request);

            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }

        public async Task<ResponceError> DeleteAuto(int id)
        {
            string url = $"{Settings.ApiRoot}/v1/autos/{id}";

            var response = await _httpClient.DeleteAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }

        public async Task<IEnumerable<CharacteristicModel>> GetColorList()
        {
            string url = $"{Settings.ApiRoot}/v1/colors";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<IEnumerable<CharacteristicModel>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new List<CharacteristicModel>();

            return data;
        }
        public async Task<IEnumerable<CharacteristicModel>> GetMarkList()
        {
            string url = $"{Settings.ApiRoot}/v1/marks";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<IEnumerable<CharacteristicModel>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new List<CharacteristicModel>();

            return data;
        }       
    }
}
