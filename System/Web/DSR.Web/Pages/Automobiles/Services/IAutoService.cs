﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Automobiles.Models;

namespace DSR.Web.Pages.Automobiles.Services
{
    public interface IAutoService
    {
        Task<IEnumerable<AutoListItem>> GetAutos(int offset = 0, int limit = 10);
        Task<AutoListItem> GetAuto(int id);
        Task<ResponceError> AddAuto(AutoModel model);
        Task<ResponceError> UpdateAuto(int id, AutoModel model);
        Task<ResponceError> DeleteAuto(int id);

        Task<IEnumerable<CharacteristicModel>> GetColorList();
        Task<IEnumerable<CharacteristicModel>> GetMarkList();
    }
}
