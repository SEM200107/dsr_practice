﻿using FluentValidation;

namespace DSR.Web.Pages.Profile.Models
{
    public class ChangeFullNameModel
    {
        public string Name { get; set; }
    }

    public class ChangeFullNameModelValidator : AbstractValidator<ChangeFullNameModel>
    {
        public ChangeFullNameModelValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("FullName is empty.")
                .MaximumLength(50).WithMessage("FullName is long.")
                .MinimumLength(4).WithMessage("FullName is short.");
        }
        public Func<object, string, Task<IEnumerable<string>>> ValidateValue => async (model, propertyName) =>
        {
            var result = await ValidateAsync(ValidationContext<ChangeFullNameModel>.CreateWithOptions((ChangeFullNameModel)model, x => x.IncludeProperties(propertyName)));
            if (result.IsValid)
                return Array.Empty<string>();
            return result.Errors.Select(e => e.ErrorMessage);
        };
    }
}
