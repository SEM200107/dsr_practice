﻿using Blazored.LocalStorage;
using DSR.Web.ErrorModel;
using DSR.Web.Pages.Profile.Models;
using System.Text;
using System.Text.Json;

namespace DSR.Web.Pages.Profile.Services
{
    public class ProfileService : IProfileService
    {
        private readonly HttpClient _httpClient;
        private readonly ILocalStorageService localStorage;

        public ProfileService(HttpClient httpClient, ILocalStorageService localStorage)
        {
            this._httpClient = httpClient;
            this.localStorage = localStorage;
        }

        public async Task<ProfileModel> GetProfile()
        {
            var token = await localStorage.GetItemAsync<string>("authToken");

            string url = $"{Settings.ApiRoot}/v1/accounts/profile/{token}";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            var result = new ProfileModel();
            if (!response.IsSuccessStatusCode)
            {
                result = JsonSerializer.Deserialize<ProfileModel>(content);
                return result;
            }

            result = JsonSerializer.Deserialize<ProfileModel>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new ProfileModel();

            return result;
        }
        public async Task EditProfileFullName(ChangeFullNameModel model)
        {
            var token = await localStorage.GetItemAsync<string>("authToken");

            string url = $"{Settings.ApiRoot}/v1/accounts/profile/editfullname/{token}/{model.Name}";

            var response = await _httpClient.GetAsync(url);
        }
        public async Task<ResponceError> EditProfileEmail(ChangeEmailModel model)
        {
            var token = await localStorage.GetItemAsync<string>("authToken");

            string url = $"{Settings.ApiRoot}/v1/accounts/profile/editemail/{token}/{model.Email}";

            var response = await _httpClient.GetAsync(url);

            var content = await response.Content.ReadAsStringAsync();

            var result = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                result = JsonSerializer.Deserialize<ResponceError>(content);
                return result;
            }
            return result;

        }
        public async Task<ResponceError> ChangePassword(ChangePasswordModel model)
        {
            var token = await localStorage.GetItemAsync<string>("authToken");

            string url = $"{Settings.ApiRoot}/v1/accounts/changepassword/{token}";

            var body = JsonSerializer.Serialize(model);
            var request = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(url, request);

            var content = await response.Content.ReadAsStringAsync();

            var result = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                result = JsonSerializer.Deserialize<ResponceError>(content);
                return result;
            }
            return result;
        }
    }
}
