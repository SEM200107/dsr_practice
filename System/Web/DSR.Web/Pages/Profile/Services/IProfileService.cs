﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Profile.Models;

namespace DSR.Web.Pages.Profile.Services
{
    public interface IProfileService
    {
        Task<ProfileModel> GetProfile();
        Task EditProfileFullName (ChangeFullNameModel model);
        Task<ResponceError> EditProfileEmail(ChangeEmailModel model);
        Task<ResponceError> ChangePassword (ChangePasswordModel model);
    }
}
