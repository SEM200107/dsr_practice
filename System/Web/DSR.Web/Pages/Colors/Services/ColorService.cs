﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Colors.Models;
using System.Text;
using System.Text.Json;

namespace DSR.Web.Pages.Colors.Services
{
    public class ColorService : IColorService
    {
        private readonly HttpClient _httpClient;

        public ColorService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<ColorListItem>> GetColors()
        {
            string url = $"{Settings.ApiRoot}/v1/colors";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<IEnumerable<ColorListItem>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new List<ColorListItem>();

            return data;
        }

        public async Task<ColorListItem> GetColor(int id)
        {
            string url = $"{Settings.ApiRoot}/v1/colors/{id}";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<ColorListItem>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new ColorListItem();

            return data;
        }

        public async Task<ResponceError> AddColor(ColorModel model)
        {
            string url = $"{Settings.ApiRoot}/v1/colors";

            var body = JsonSerializer.Serialize(model);
            var request = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(url, request);

            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }

        public async Task<ResponceError> UpdateColor(int id, ColorModel model)
        {
            string url = $"{Settings.ApiRoot}/v1/colors/{id}";

            var body = JsonSerializer.Serialize(model);
            var request = new StringContent(body, Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(url, request);

            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }
        
        public async Task<ResponceError> DeleteColor(int id)
        {
            string url = $"{Settings.ApiRoot}/v1/colors/{id}";

            var response = await _httpClient.DeleteAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }
    }
}
