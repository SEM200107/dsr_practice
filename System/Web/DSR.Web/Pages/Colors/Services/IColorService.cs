﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Colors.Models;

namespace DSR.Web.Pages.Colors.Services
{
    public interface IColorService
    {
        Task<IEnumerable<ColorListItem>> GetColors();
        Task<ColorListItem> GetColor(int id);
        Task<ResponceError> AddColor(ColorModel model);
        Task<ResponceError> UpdateColor(int id, ColorModel model);
        Task<ResponceError> DeleteColor(int id);
    }
}
