﻿namespace DSR.Web.Pages.Colors.Models
{
    public class ColorListItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
