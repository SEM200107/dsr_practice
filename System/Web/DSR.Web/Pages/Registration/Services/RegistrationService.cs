﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Registration.Models;
using System.Text;
using System.Text.Json;

namespace DSR.Web.Pages.Registration.Services
{
    public class RegistrationService: IRegistrationService
    {
        private readonly HttpClient _httpClient;

        public RegistrationService(HttpClient _httpClient)
        {
            this._httpClient = _httpClient;
        }
        public async Task<ResponceError> CreateUser(RegistrationModel model)
        {
            string url = $"{Settings.ApiRoot}/v1/accounts";

            var body = JsonSerializer.Serialize(model);
            var request = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(url, request);

            var content = await response.Content.ReadAsStringAsync();

            var result = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                result = JsonSerializer.Deserialize<ResponceError>(content);
                return result;
            }
            return result;
        }

        public async Task<bool> CheckConfirmEmail (string email)
        {
            string url = $"{Settings.ApiRoot}/v1/accounts/{email}";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<bool>(content);
            
            return data;
        }
    }
}
