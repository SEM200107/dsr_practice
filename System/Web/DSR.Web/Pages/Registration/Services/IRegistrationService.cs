﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Registration.Models;

namespace DSR.Web.Pages.Registration.Services
{
    public interface IRegistrationService
    {
        Task<ResponceError> CreateUser (RegistrationModel model);
        Task<bool> CheckConfirmEmail (string email);
    }
}
