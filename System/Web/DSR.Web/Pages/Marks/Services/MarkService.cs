﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Marks.Models;
using System.Text;
using System.Text.Json;

namespace DSR.Web.Pages.Marks.Services
{
    public class MarkService : IMarkService
    {
        private readonly HttpClient _httpClient;

        public MarkService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<MarkListItem>> GetMarks()
        {
            string url = $"{Settings.ApiRoot}/v1/marks";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<IEnumerable<MarkListItem>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new List<MarkListItem>();

            return data;
        }

        public async Task<MarkListItem> GetMark(int id)
        {
            string url = $"{Settings.ApiRoot}/v1/marks/{id}";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<MarkListItem>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new MarkListItem();

            return data;
        }

        public async Task<ResponceError> AddMark(MarkModel model)
        {
            string url = $"{Settings.ApiRoot}/v1/marks";

            var body = JsonSerializer.Serialize(model);
            var request = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(url, request);

            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }

        public async Task<ResponceError> UpdateMark(int id, MarkModel model)
        {
            string url = $"{Settings.ApiRoot}/v1/marks/{id}";

            var body = JsonSerializer.Serialize(model);
            var request = new StringContent(body, Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(url, request);

            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }

        public async Task<ResponceError> DeleteMark(int id)
        {
            string url = $"{Settings.ApiRoot}/v1/marks/{id}";

            var response = await _httpClient.DeleteAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            var error = new ResponceError();
            if (!response.IsSuccessStatusCode)
            {
                error = JsonSerializer.Deserialize<ResponceError>(content);
                if (error.ErrorCode == -1)
                    error.Message = "Operation failed";
            }
            return error;
        }
        
        public async Task<IEnumerable<ModelModel>> GetModelList()
        {
            string url = $"{Settings.ApiRoot}/v1/models";

            var response = await _httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }

            var data = JsonSerializer.Deserialize<IEnumerable<ModelModel>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new List<ModelModel>();

            return data;
        }
    }
}
