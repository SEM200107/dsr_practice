﻿using DSR.Web.ErrorModel;
using DSR.Web.Pages.Marks.Models;

namespace DSR.Web.Pages.Marks.Services
{
    public interface IMarkService
    {
        Task<IEnumerable<MarkListItem>> GetMarks();
        Task<MarkListItem> GetMark(int id);
        Task<ResponceError> AddMark(MarkModel model);
        Task<ResponceError> UpdateMark(int id, MarkModel model);
        Task<ResponceError> DeleteMark(int id);

        Task<IEnumerable<ModelModel>> GetModelList();
    }
}
