﻿using FluentValidation;

namespace DSR.Web.Pages.Marks.Models
{
    public class MarkModel
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public int ModelId { get; set; }
    }
    public class MarkModelValidator : AbstractValidator<MarkModel>
    {
        public MarkModelValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");
            RuleFor(x => x.ModelId)
                .GreaterThan(0).WithMessage("Please, select an model");
        }
        public Func<object, string, Task<IEnumerable<string>>> ValidateValue => async (model, propertyName) =>
        {
            var result = await ValidateAsync(ValidationContext<MarkModel>.CreateWithOptions((MarkModel)model, x => x.IncludeProperties(propertyName)));
            if (result.IsValid)
                return Array.Empty<string>();
            return result.Errors.Select(e => e.ErrorMessage);
        };
    }
}
