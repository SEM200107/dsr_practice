﻿namespace DSR.Web.Pages.Marks.Models
{
    public class MarkListItem
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Model { get; set; } = string.Empty;
        public int ModelId { get; set; }
    }
}
