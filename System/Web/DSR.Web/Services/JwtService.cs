﻿using Blazored.LocalStorage;

namespace DSR.Web.Services
{
    public class JwtService : IJwtService
    {
        private readonly HttpClient httpClient;
        private readonly ILocalStorageService localStorage;

        public JwtService(HttpClient httpClient, ILocalStorageService localStorage)
        {
            this.httpClient = httpClient;
            this.localStorage = localStorage;
        }
        public async Task<bool> CheckLifetime()
        {
            var token = await localStorage.GetItemAsync<string>("authToken");

            string url = $"{Settings.ApiRoot}/v1/accounts/lifetimetoken/{token}";

            var response = await httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            content = content.Remove(0,1);
            content = content.Remove(content.Length - 1, 1);
            var lifetime = Convert.ToDateTime(content);
            var date = DateTime.Now;
            if (date > lifetime)
            {
                await localStorage.RemoveItemAsync("authToken");
                await localStorage.RemoveItemAsync("refreshToken");
                return false;
            }
            return true;
            
        }
    }
}
