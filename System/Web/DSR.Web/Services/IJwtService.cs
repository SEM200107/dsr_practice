﻿namespace DSR.Web.Services
{
    public interface IJwtService
    {
        Task<bool> CheckLifetime();
    }
}
