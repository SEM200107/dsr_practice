﻿namespace DSR.Identity;

using DSR.Shared.Common.Security;
using Duende.IdentityServer.Models;

public static class AppApiScopes
{
    public static IEnumerable<ApiScope> ApiScopes =>
        new List<ApiScope>
        {
            new ApiScope(AppScopes.AutomobilesRead, "Access to books API - Read data"),
            new ApiScope(AppScopes.AutomobilesWrite, "Access to books API - Write data")
        };
}