﻿namespace DSR.Identity;

using DSR.Settings;

public static class Bootstrapper
{
    public static void AddAppServices(this IServiceCollection services)
    {
        services.AddSettings();
    }
}