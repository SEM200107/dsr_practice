﻿namespace DSR.Worker;

using DSR.EmailService;
using DSR.RabbitMQService;
using DSR.Settings;
using Microsoft.Extensions.DependencyInjection;

public static class Bootstrapper
{
    public static IServiceCollection RegisterServices(this IServiceCollection services)
    {
        services
            .AddSettings()
            .AddEmailSender()
            .AddRabbitMq()
            .AddSingleton<ITaskExecutor, TaskExecutor>();

        return services;
    }
}
 



