﻿namespace DSR.Worker;

using DSR.Db.Context.Context;
using DSR.Db.Context.Factories;
using DSR.Db.Context.Setup;
using DSR.Settings;

public static class DbContextConfiguration
{
    public static IServiceCollection AddAppDbContext(this IServiceCollection services, IWorkerSettings settings)
    {
        var dbOptionsDelegate = DbContextOptionFactory.Configure(settings.Db.ConnectionString);

        services.AddDbContextFactory<MainDbContext>(dbOptionsDelegate, ServiceLifetime.Singleton);

        return services;
    }

    public static WebApplication UseAppDbContext(this WebApplication app)
    {
        return app;
    }
}