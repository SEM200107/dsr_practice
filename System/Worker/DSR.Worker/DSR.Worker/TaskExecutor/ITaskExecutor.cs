﻿namespace DSR.Worker;

public interface ITaskExecutor
{
    void Start();
}
