﻿using DSR.AutoService;
using DSR.EmailService;
using DSR.RabbitMQService;
using DSR.Settings;
using DSR.UserAccount;

namespace DSR.Api
{
    public static class Bootstrapper
    {
        public static void AddAppServices(this IServiceCollection services)
        {
            services
                .AddSettings()
                .AddAutoService()
                .AddEmailSender()
                .AddRabbitMq()
                .AddUserAccountService();
        }
    }
}
