using DSR.Api;
using DSR.Api.Configuration;
using DSR.Settings;
using Serilog;

var builder = WebApplication.CreateBuilder(args);


builder.Host.UseSerilog((host, cfg) =>
{
    cfg.ReadFrom.Configuration(host.Configuration);
});

var settings = new ApiSettings(new SettingsSource());


var services = builder.Services;

services.AddHttpContextAccessor();

services.AddAppDbContext(settings);

services.AddAppHealthCheck();

services.AddAppVersions();

services.AddAppSwagger(settings);

services.AddAppCors();

services.AddAppServices();

services.AddAppAuth(settings);

services.AddControllers().AddValidator();

services.AddRazorPages();

services.AddAutoMappers();



var app = builder.Build();

Log.Information("Starting up");

app.UseAppMiddlewares();

app.UseStaticFiles();

app.UseRouting();

app.UseAppCors();

app.UseAppHealthCheck();

app.UseSerilogRequestLogging();

app.UseAppSwagger();

app.UseAppAuth();

app.MapRazorPages();

app.MapControllers();

app.UseAppDbContext();

app.Run();
