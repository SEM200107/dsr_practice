﻿using DSR.Db.Context.Context;
using DSR.Db.Context.Factories;
using DSR.Db.Context.Setup;
using DSR.Settings;

namespace DSR.Api.Configuration
{
    public static class DbConfiguration
    {
        public static IServiceCollection AddAppDbContext (this IServiceCollection services, IApiSettings settings)
        {
            var dbOptionsDelegate = DbContextOptionFactory.Configure(settings.Db.ConnectionString);

            services.AddDbContextFactory<MainDbContext>(dbOptionsDelegate, ServiceLifetime.Singleton);
            return services;
        }

        public static IApplicationBuilder UseAppDbContext (this IApplicationBuilder app)
        {
            DbInit.Execute(app.ApplicationServices);
            DbSeed.Execute(app.ApplicationServices);
            return app;
        }
    }
}
