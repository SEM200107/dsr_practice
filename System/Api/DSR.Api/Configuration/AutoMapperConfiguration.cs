﻿using DSR.Shared.Common.Helpers;

namespace DSR.Api.Configuration
{
    public static class AutoMapperConfiguration
    {
        public static IServiceCollection AddAutoMappers(this IServiceCollection services)
        {
            AutoMappersRegisterHelper.Register(services);

            return services;
        }
    }
}
