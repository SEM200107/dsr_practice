﻿using AutoMapper;
using DSR.UserAccount.Models;
using FluentValidation;

namespace DSR.Api.Controllers.Accounts.Models
{
    public class ChangePasswordRequest
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
    public class ChangePasswordRequestValidator : AbstractValidator<ChangePasswordRequest>
    {
        public ChangePasswordRequestValidator()
        {
            RuleFor(x => x.NewPassword)
                .NotEmpty().WithMessage("Password is required.")
                .MaximumLength(50).WithMessage("Password is long.")
                .MinimumLength(8).WithMessage("Password is short.");
        }
    }

    public class ChangePasswordRequestProfile : Profile
    {
        public ChangePasswordRequestProfile()
        {
            CreateMap<ChangePasswordRequest, ChangePasswordModel>();
        }
    }
}
