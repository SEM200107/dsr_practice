﻿using AutoMapper;
using DSR.UserAccount.Models;
using FluentValidation;

namespace DSR.Api.Controllers.Accounts.Models
{
    public class ResetPasswordUserRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Code { get; set; }
    }
    public class ResetPasswordUserRequestValidator : AbstractValidator<ResetPasswordUserRequest>
    {
        public ResetPasswordUserRequestValidator()
        {
            RuleFor(x => x.Email)
                .MaximumLength(50).WithMessage("Email is long.")
                .EmailAddress().WithMessage("Email is required.");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Password is required.")
                .MaximumLength(50).WithMessage("Password is long.")
                .MinimumLength(8).WithMessage("Password is short.");
        }
    }
    public class ResetPasswordUserRequestProfile : Profile
    {
        public ResetPasswordUserRequestProfile()
        {
            CreateMap<ResetPasswordUserRequest, ResetPasswordUserModel>();
        }
    }
}
