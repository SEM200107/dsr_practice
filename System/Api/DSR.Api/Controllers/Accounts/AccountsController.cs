﻿namespace DSR.Api.Controllers.Accounts;

using AutoMapper;
using DSR.Api.Controllers.Accounts.Models;
using DSR.Settings;
using DSR.Shared.Common.Security;
using DSR.UserAccount;
using DSR.UserAccount.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[Route("api/v{version:apiVersion}/accounts")]
[ApiController]
[ApiVersion("1.0")]
public class AccountsController : ControllerBase
{
    private readonly IMapper mapper;
    private readonly ILogger<AccountsController> logger;
    private readonly IUserAccountService userAccountService;
    private readonly IGeneralSettings generalSettings;

    public AccountsController(IMapper mapper, ILogger<AccountsController> logger, IUserAccountService userAccountService, IGeneralSettings generalSettings)
    {
        this.mapper = mapper;
        this.logger = logger;
        this.userAccountService = userAccountService;
        this.generalSettings = generalSettings;
    }

    [HttpPost("")]
    public async Task<UserAccountResponse> Register([FromBody] RegisterUserAccountRequest request)
    {
        var user = await userAccountService.Create(mapper.Map<RegisterUserAccountModel>(request));

        var response = mapper.Map<UserAccountResponse>(user);

        return response;
    }

    [HttpPost("resetpassword")]
    public async Task ResetPassword ([FromBody] ResetPasswordUserRequest request)
    {
        await userAccountService.ResetPassword(mapper.Map<ResetPasswordUserModel>(request));
    }

    [HttpGet("resetpassword/confirm")]
    public async Task<IActionResult> ConfirmResetPassword([FromQuery] Guid user, [FromQuery] string code, [FromQuery] string password)
    {
        await userAccountService.ConfirmResetPassword(user, code, password);
        return Redirect(generalSettings.MainUrlWeb.TrimEnd('/') + "/confirmresetpassword");
    }

    [HttpGet("email/confirm")]
    public async Task<IActionResult> ConfirmEmail([FromQuery] Guid user, [FromQuery] string code)
    {
        await userAccountService.ConfirmEmail(user, code);
        return Redirect(generalSettings.MainUrlWeb.TrimEnd('/') + "/confirmemail");
    }

    [Authorize(AppScopes.AutomobilesRead)]
    [HttpGet("{email}")]
    public async Task<bool> CheckConfirmEmail([FromRoute] string email)
    {
        return await userAccountService.CheckConfirmEmail(email);
    }

    [Authorize(AppScopes.AutomobilesRead)]
    [HttpGet("profile/{token}")]
    public async Task<UserAccountResponse> GetUser ([FromRoute] string token)
    {
        var user = await userAccountService.GetUser(token);

        var response = mapper.Map<UserAccountResponse>(user);

        return response;
    }

    [Authorize(AppScopes.AutomobilesRead)]
    [HttpGet("profile/editfullname/{token}/{fullName}")] 
    public async Task EditUserFullName ([FromRoute] string token, [FromRoute] string fullName)
    {
        await userAccountService.EditUserFullName(token, fullName);
    }

    [Authorize(AppScopes.AutomobilesRead)]
    [HttpGet("profile/editemail/{token}/{email}")]
    public async Task EditUserEmail([FromRoute] string token, [FromRoute] string email)
    {
        await userAccountService.EditUserEmail(token, email);
    }

    [Authorize(AppScopes.AutomobilesRead)]
    [HttpPost("changepassword/{token}")]
    public async Task ChangePassword ([FromRoute] string token, [FromBody] ChangePasswordRequest request)
    {
        await userAccountService.ChangePassword(token, mapper.Map<ChangePasswordModel>(request));
    }

    [HttpGet("lifetimetoken/{token}")]
    public async Task<DateTime> LifetimeAccessToken ([FromRoute] string token)
    {
        return await userAccountService.LifetimeAccessToken(token);
    }
}
