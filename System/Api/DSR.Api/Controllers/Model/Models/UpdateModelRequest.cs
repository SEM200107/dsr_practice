﻿using AutoMapper;
using DSR.Services.Models.Model;
using FluentValidation;

namespace DSR.Api.Controllers.Model.Models
{
    public class UpdateModelRequest
    {
        public string Title { get; set; } = string.Empty;
    }
    public class UpdateModelRequestValidator : AbstractValidator<UpdateModelRequest>
    {
        public UpdateModelRequestValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");
        }
    }
    public class UpdateModelRequestProfile : Profile
    {
        public UpdateModelRequestProfile()
        {
            CreateMap<UpdateModelRequest, UpdateModelModel>();
        }
    }
}
