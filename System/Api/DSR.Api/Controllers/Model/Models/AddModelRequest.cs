﻿using AutoMapper;
using DSR.Services.Models.Model;
using FluentValidation;

namespace DSR.Api.Controllers.Model.Models
{
    public class AddModelRequest
    {
        public string Title { get; set; } = string.Empty;
    }
    public class AddModelRequestValidator : AbstractValidator<AddModelRequest>
    {
        public AddModelRequestValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");
        }
    }
    public class AddModelRequestProfile : Profile
    {
        public AddModelRequestProfile()
        {
            CreateMap<AddModelRequest, AddModelModel>();
        }
    }
}
