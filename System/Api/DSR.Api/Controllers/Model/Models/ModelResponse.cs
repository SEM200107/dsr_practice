﻿using AutoMapper;
using DSR.Services.Models.Model;

namespace DSR.Api.Controllers.Model.Models
{
    public class ModelResponse
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
    }
    public class ModelResponseProfile : Profile
    {
        public ModelResponseProfile()
        {
            CreateMap<ModelModel, ModelResponse>();
        }
    }
}
