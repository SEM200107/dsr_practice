﻿using AutoMapper;
using DSR.Services.Models;
using DSR.Services.Models.Model;
using DSR.Shared.Common.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DSR.Api.Controllers.Model.Models
{
    [ApiController]
    [Route("api/v{version:apiVersion}/models")]
    [ApiVersion("1.0")]
    public class ModelController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly ILogger<ModelController> logger;
        private readonly IModelService modelService;

        public ModelController(IMapper mapper, ILogger<ModelController> logger, IModelService modelService)
        {
            this.mapper = mapper;
            this.logger = logger;
            this.modelService = modelService;
        }

        [HttpGet("")]
        [Authorize(AppScopes.AutomobilesRead)]
        public async Task<IEnumerable<ModelResponse>> GetModels()
        {
            var modelAuto = await modelService.GetModels();
            var response = mapper.Map<IEnumerable<ModelResponse>>(modelAuto);

            return response;
        }

        [HttpGet("{id}")]
        [Authorize(AppScopes.AutomobilesRead)]
        public async Task<ModelResponse> GetModelById([FromRoute] int id)
        {
            var modelAuto = await modelService.GetModel(id);
            var response = mapper.Map<ModelResponse>(modelAuto);

            return response;
        }

        [HttpPost("")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<ModelResponse> AddModel([FromBody] AddModelRequest request)
        {
            var model = mapper.Map<AddModelModel>(request);
            var modelAuto = await modelService.AddModel(model);
            var response = mapper.Map<ModelResponse>(modelAuto);

            return response;
        }

        [HttpPut("{id}")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<IActionResult> UpdateModel([FromRoute] int id, [FromBody] UpdateModelRequest request)
        {
            var model = mapper.Map<UpdateModelModel>(request);
            await modelService.UpdateModel(id, model);
            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<IActionResult> DeleteModel([FromRoute] int id)
        {
            await modelService.DeleteModel(id);
            return Ok();
        }
    }
}
