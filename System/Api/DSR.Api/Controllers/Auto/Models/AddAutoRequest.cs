﻿using AutoMapper;
using FluentValidation;

namespace DSR.Api.Controllers.Auto
{
    public class AddAutoRequest
    {
        public string Number { get; set; } = string.Empty;        
        public string Description { get; set; } = string.Empty;
        public int ColorId { get; set; } 
        public int MarkId { get; set; } 
    }
    
    public class AddAutoResponseValidator : AbstractValidator<AddAutoRequest>
    {
        public AddAutoResponseValidator()
        {
            RuleFor(x => x.Number)
                .NotEmpty().WithMessage("Number is required")
                .MaximumLength(9).WithMessage("Number is long");
            RuleFor(x => x.ColorId)
                .NotEmpty().WithMessage("Calor is required");
            RuleFor(x => x.Description)
                .NotEmpty().WithMessage("Description is required")
                .MaximumLength(1000).WithMessage("Description is long");
            RuleFor(x => x.MarkId)
                .NotEmpty().WithMessage("Mark is required");
        }
    }
    public class AddAutoRequestProfile : Profile
    {
        public AddAutoRequestProfile()
        {
            CreateMap<AddAutoRequest, AddAutoModel>();
        }
    }
}
