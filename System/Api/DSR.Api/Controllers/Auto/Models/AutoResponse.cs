﻿using AutoMapper;
using DSR.Db.Entities;

namespace DSR.Api.Controllers.Auto
{
    public class AutoResponse
    {
        public int Id { get; set; }
        public string Number { get; set; } = string.Empty;        
        public string Description { get; set; } = string.Empty;
        public string Color { get; set; } = string.Empty;
        public int ColorId { get; set; }
        public string Model { get; set; } = string.Empty;
        public int ModelId { get; set; }
        public string Mark { get; set; } = string.Empty;
        public int MarkId { get; set; }
    }
    public class AutoResponseProfile : Profile
    {
        public AutoResponseProfile()
        {
            CreateMap<AutoModel, AutoResponse>();
        }
    }
}
