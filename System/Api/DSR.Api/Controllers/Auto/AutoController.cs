﻿using AutoMapper;
using DSR.Api.Controllers.Auto;
using DSR.AutoService;
using DSR.Shared.Common.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DSR.Api.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/autos")]
    [ApiVersion("1.0")]
    public class AutoController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly ILogger<AutoController> logger;
        private readonly IAutoService autoService;

        public AutoController(IMapper mapper, ILogger<AutoController> logger, IAutoService autoService)
        {
            this.mapper = mapper;
            this.logger = logger;
            this.autoService = autoService;
        }

        [HttpGet("")]
        [Authorize(AppScopes.AutomobilesRead)]
        public async Task<IEnumerable<AutoResponse>> GetAutos([FromQuery] int offset = 0, [FromQuery] int limit = 10)
        {
            var automobiles = await autoService.GetAutos(offset, limit);            
            var response = mapper.Map<IEnumerable<AutoResponse>>(automobiles);

            return response;
        }

        [HttpGet("{id}")]
        [Authorize(AppScopes.AutomobilesRead)]
        public async Task<AutoResponse> GetAutoById([FromRoute]int id)
        {
            var automobile = await autoService.GetAuto(id);
            var response = mapper.Map<AutoResponse>(automobile);

            return response;
        }

        [HttpPost("")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<AutoResponse> AddAuto([FromBody] AddAutoRequest request)
        {
            var model = mapper.Map<AddAutoModel>(request);
            var automobile = await autoService.AddAuto(model);
            var response = mapper.Map<AutoResponse>(automobile);

            return response;
        }

        [HttpPut("{id}")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<IActionResult> UpdateAuto([FromRoute] int id, [FromBody] UpdateAutoRequest request)
        {
            var model = mapper.Map<UpdateAutoModel>(request);
            await autoService.UpdateAuto(id,model);
            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<IActionResult> DeleteAuto([FromRoute] int id)
        {
            await autoService.DeleteAuto(id);
            return Ok();
        }
    }
}
