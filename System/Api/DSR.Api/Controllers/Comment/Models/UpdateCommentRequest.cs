﻿using AutoMapper;
using DSR.AutoServices.Comments.Models;
using FluentValidation;

namespace DSR.Api.Controllers.Comment.Models
{
    public class UpdateCommentRequest
    {
        public string Content { get; set; }
        public Guid UserId { get; set; }
        public int AutomobileId { get; set; }
    }
    public class UpdateCommentRequestValidator : AbstractValidator<UpdateCommentRequest>
    {
        public UpdateCommentRequestValidator()
        {
            RuleFor(x => x.Content)
                .NotEmpty().WithMessage("Content is required")
                .MaximumLength(500).WithMessage("Content is long");
            RuleFor(x => x.UserId)
                .NotEmpty().WithMessage("User is required");
            RuleFor(x => x.AutomobileId)
                .NotEmpty().WithMessage("Automobile is required");
        }
    }
    public class UpdateCommentRequestProfile : Profile
    {
        public UpdateCommentRequestProfile()
        {
            CreateMap<UpdateCommentRequest, UpdateCommentModel>();
        }
    }
}
