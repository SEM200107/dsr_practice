﻿using AutoMapper;
using DSR.AutoServices.Comments.Models;

namespace DSR.Api.Controllers.Comment.Models
{
    public class CommentResponse
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public Guid UserId { get; set; }
        public string FullNameUser { get; set; }
        public int AutomobileId { get; set; }
    }
    public class CommentResponseProfile : Profile
    {
        public CommentResponseProfile()
        {
            CreateMap<CommentModel, CommentResponse>();
        }
    }
}
