﻿using AutoMapper;
using DSR.Api.Controllers.Comment.Models;
using DSR.AutoServices.Comments;
using DSR.AutoServices.Comments.Models;
using DSR.Shared.Common.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DSR.Api.Controllers.Comment
{
    [ApiController]
    [Route("api/v{version:apiVersion}/comments")]
    [ApiVersion("1.0")]
    public class CommentController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly ILogger<CommentController> logger;
        private readonly ICommentService commentService;

        public CommentController(IMapper mapper, ILogger<CommentController> logger, ICommentService commentService)
        {
            this.mapper = mapper;
            this.logger = logger;
            this.commentService = commentService;
        }

        [HttpGet("{idAuto}")]
        [Authorize(AppScopes.AutomobilesRead)]
        public async Task<IEnumerable<CommentResponse>> GetComments([FromRoute] int idAuto)
        {
            var comments = await commentService.GetCommentsByAutoId(idAuto);
            var response = mapper.Map<IEnumerable<CommentResponse>>(comments);

            return response;
        }

        [HttpGet("getcomment/{id}")]
        [Authorize(AppScopes.AutomobilesRead)]
        public async Task<CommentResponse> GetComment([FromRoute] int id)
        {
            var automobile = await commentService.GetCommentById(id);
            var response = mapper.Map<CommentResponse>(automobile);

            return response;
        }

        [HttpPost("")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<CommentResponse> AddComment([FromBody] AddCommentRequest request)
        {
            var model = mapper.Map<AddCommentModel>(request);
            var comment = await commentService.AddComment(model);
            var response = mapper.Map<CommentResponse>(comment);

            return response;
        }

        [HttpPut("{id}")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<IActionResult> CommentAuto([FromRoute] int id, [FromBody] UpdateCommentRequest request)
        {
            var model = mapper.Map<UpdateCommentModel>(request);
            await commentService.UpdateComment(id, model);
            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<IActionResult> DeleteComment([FromRoute] int id)
        {
            await commentService.DeleteComment(id);
            return Ok();
        }
    }
}
