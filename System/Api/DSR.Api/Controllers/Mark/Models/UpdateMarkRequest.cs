﻿using AutoMapper;
using DSR.AutoServices.Marks.Models;
using FluentValidation;

namespace DSR.Api.Controllers.Mark.Models
{
    public class UpdateMarkRequest
    {
        public string Title { get; set; } = string.Empty;
        public int ModelId { get; set; }
    }
    public class UpdateMarkRequestValidator : AbstractValidator<UpdateMarkRequest>
    {
        public UpdateMarkRequestValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");
            RuleFor(x => x.ModelId)
                .NotEmpty().WithMessage("Modelid is required");
        }
    }
    public class UpdateMarkRequestProfile : Profile
    {
        public UpdateMarkRequestProfile()
        {
            CreateMap<UpdateMarkRequest, UpdateMarkModel>();
        }
    }
}
