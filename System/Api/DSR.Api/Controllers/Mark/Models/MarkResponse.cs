﻿using AutoMapper;
using DSR.Services.Marks.Model;

namespace DSR.Api.Controllers.Mark.Models
{
    public class MarkResponse
    {        
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public int ModelId { get; set; }
        public string Model { get; set; } = string.Empty;        
    }
    public class MarkResponseProfile : Profile
    {
        public MarkResponseProfile()
        {
            CreateMap<MarkModel, MarkResponse>();
        }
    }
}
