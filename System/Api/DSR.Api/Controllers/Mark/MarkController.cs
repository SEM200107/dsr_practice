﻿using AutoMapper;
using DSR.Api.Controllers.Mark.Models;
using DSR.AutoServices.Marks.Models;
using DSR.Services.Marks;
using DSR.Shared.Common.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DSR.Api.Controllers.Mark
{
    [ApiController]
    [Route("api/v{version:apiVersion}/marks")]
    [ApiVersion("1.0")]
    public class MarkController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly ILogger<MarkController> logger;
        private readonly IMarkService markService;

        public MarkController(IMapper mapper, ILogger<MarkController> logger, IMarkService markService)
        {
            this.mapper = mapper;
            this.logger = logger;
            this.markService = markService;
        }

        [HttpGet("")]
        [Authorize(AppScopes.AutomobilesRead)]
        public async Task<IEnumerable<MarkResponse>> GetMarks()
        {
            var marks = await markService.GetMarks();
            var response = mapper.Map<IEnumerable<MarkResponse>>(marks);

            return response;
        }

        [HttpGet("{id}")]
        [Authorize(AppScopes.AutomobilesRead)]
        public async Task<MarkResponse> GetMarkById([FromRoute] int id)
        {
            var mark = await markService.GetMark(id);
            var response = mapper.Map<MarkResponse>(mark);

            return response;
        }

        [HttpPost("")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<MarkResponse> AddMark([FromBody] AddMarkRequest request)
        {
            var model = mapper.Map<AddMarkModel>(request);
            var mark = await markService.AddMark(model);
            var response = mapper.Map<MarkResponse>(mark);

            return response;
        }

        [HttpPut("{id}")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<IActionResult> UpdateMark([FromRoute] int id, [FromBody] UpdateMarkRequest request)
        {
            var model = mapper.Map<UpdateMarkModel>(request);
            await markService.UpdateMark(id, model);
            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<IActionResult> DeleteMark([FromRoute] int id)
        {
            await markService.DeleteMark(id);
            return Ok();
        }
    }
}
