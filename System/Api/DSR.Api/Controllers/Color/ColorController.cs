﻿using AutoMapper;
using DSR.Api.Controllers.Color.Models;
using DSR.AutoServices.Colors.Models;
using DSR.Services.Calors;
using DSR.Shared.Common.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DSR.Api.Controllers.Color
{
    [ApiController]
    [Route("api/v{version:apiVersion}/colors")]
    [ApiVersion("1.0")]
    public class ColorController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly ILogger<ColorController> logger;
        private readonly IColorService colorService;

        public ColorController(IMapper mapper, ILogger<ColorController> logger, IColorService colorService)
        {
            this.mapper = mapper;
            this.logger = logger;
            this.colorService = colorService;
        }

        [HttpGet("")]
        [Authorize(AppScopes.AutomobilesRead)]
        public async Task<IEnumerable<ColorResponse>> GetColors()
        {
            var color = await colorService.GetColors();
            var response = mapper.Map<IEnumerable<ColorResponse>>(color);

            return response;
        }

        [HttpGet("{id}")]
        [Authorize(AppScopes.AutomobilesRead)]
        public async Task<ColorResponse> GetColorById([FromRoute] int id)
        {
            var color = await colorService.GetColor(id);
            var response = mapper.Map<ColorResponse>(color);

            return response;
        }

        [HttpPost("")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<ColorResponse> AddColor([FromBody] AddColorRequest request)
        {
            var model = mapper.Map<AddColorModel>(request);
            var color = await colorService.AddColor(model);
            var response = mapper.Map<ColorResponse>(color);

            return response;
        }

        [HttpPut("{id}")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<IActionResult> UpdateColor([FromRoute] int id, [FromBody] UpdateColorRequest request)
        {
            var model = mapper.Map<UpdateColorModel>(request);
            await colorService.UpdateColor(id, model);
            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(AppScopes.AutomobilesWrite)]
        public async Task<IActionResult> DeleteColor([FromRoute] int id)
        {
            await colorService.DeleteColor(id);
            return Ok();
        }
    }
}
