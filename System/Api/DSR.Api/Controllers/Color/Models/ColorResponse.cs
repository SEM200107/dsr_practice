﻿using AutoMapper;
using DSR.Services.Calors.Model;

namespace DSR.Api.Controllers.Color.Models
{
    public class ColorResponse
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
    }
    public class ColorResponseProfile : Profile
    {
        public ColorResponseProfile()
        {
            CreateMap<ColorModel, ColorResponse>();
        }
    }
}
