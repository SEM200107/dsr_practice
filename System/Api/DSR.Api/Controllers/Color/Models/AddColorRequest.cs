﻿using AutoMapper;
using DSR.AutoServices.Colors.Models;
using FluentValidation;

namespace DSR.Api.Controllers.Color.Models
{
    public class AddColorRequest
    {
        public string Title { get; set; } = string.Empty;
    }
    public class AddColorRequestValidator : AbstractValidator<AddColorRequest>
    {
        public AddColorRequestValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");            
        }
    }
    public class AddColorRequestProfile : Profile
    {
        public AddColorRequestProfile()
        {
            CreateMap<AddColorRequest, AddColorModel>();
        }
    }
}
