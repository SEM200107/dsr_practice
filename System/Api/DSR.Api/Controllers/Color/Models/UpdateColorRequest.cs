﻿using AutoMapper;
using DSR.AutoServices.Colors.Models;
using FluentValidation;

namespace DSR.Api.Controllers.Color.Models
{
    public class UpdateColorRequest
    {
        public string Title { get; set; } = string.Empty;
    }
    public class UpdateColorRequestValidator : AbstractValidator<UpdateColorRequest>
    {
        public UpdateColorRequestValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");
        }
    }
    public class UpdateColorRequestProfile : Profile
    {
        public UpdateColorRequestProfile()
        {
            CreateMap<UpdateColorRequest, UpdateColorModel>();
        }
    }
}

