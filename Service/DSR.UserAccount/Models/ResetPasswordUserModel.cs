﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.UserAccount.Models
{
    public class ResetPasswordUserModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Code { get; set; }
    }
    public class ResetPasswordUserModelValidator : AbstractValidator<ResetPasswordUserModel>
    {
        public ResetPasswordUserModelValidator()
        {
            RuleFor(x => x.Email)
                .MaximumLength(50).WithMessage("Email is long.")
                .EmailAddress().WithMessage("Email is required.");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Password is required.")
                .MaximumLength(50).WithMessage("Password is long.")
                .MinimumLength(8).WithMessage("Password is short.");
        }
    }
}
