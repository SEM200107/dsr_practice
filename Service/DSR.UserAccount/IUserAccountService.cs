namespace DSR.UserAccount;

using DSR.Db.Entities;
using DSR.UserAccount.Models;

public interface IUserAccountService
{
    Task<UserAccountModel> Create (RegisterUserAccountModel model);
    Task ConfirmEmail(Guid userId, string code);
    Task<bool> CheckConfirmEmail(string email);
    Task ResetPassword(ResetPasswordUserModel model);
    Task ConfirmResetPassword(Guid userId, string code, string password);
    Task<UserAccountModel> GetUser(string token);
    Task EditUserFullName(string token, string fullName);
    Task EditUserEmail(string token, string email);
    Task ChangePassword(string token, ChangePasswordModel model);
    Task<DateTime> LifetimeAccessToken (string token);
}
