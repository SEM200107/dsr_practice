namespace DSR.UserAccount;

using AutoMapper;
using DSR.Db.Entities.User;
using DSR.RabbitMQService;
using DSR.Settings;
using DSR.Shared.Common.Extensions;
using DSR.Shared.Common.Validator;
using DSR.UserAccount.Models;
using Microsoft.AspNetCore.Identity;
using System.IdentityModel.Tokens.Jwt;

public class UserAccountService : IUserAccountService
{
    private readonly IMapper mapper;
    private readonly UserManager<User> userManager;
    private readonly IRabbitMqTask rabbitMqTask;
    private readonly IModelValidator<RegisterUserAccountModel> registerUserAccountModelValidator;
    private readonly IGeneralSettings generalSettings;

    public UserAccountService(
        IMapper mapper,
        UserManager<User> userManager,
        IRabbitMqTask rabbitMqTask,
        IModelValidator<RegisterUserAccountModel> registerUserAccountModelValidator,
        IGeneralSettings generalSettings
        )
    {
        this.mapper = mapper;
        this.userManager = userManager;
        this.rabbitMqTask = rabbitMqTask;
        this.registerUserAccountModelValidator = registerUserAccountModelValidator;
        this.generalSettings = generalSettings;
    }

    public async Task<UserAccountModel> Create(RegisterUserAccountModel model)
    {
        registerUserAccountModelValidator.Check(model);

        var user = await userManager.FindByEmailAsync(model.Email);
        if (user != null)
            throw new ProcessException($"User account with email {model.Email} already exist.");

        user = new User()
        {
            Status = UserStatus.Active,
            FullName = model.Name,
            UserName = model.Email,
            Email = model.Email,
            EmailConfirmed = false,
            PhoneNumber = null,
            PhoneNumberConfirmed = false
        };

        var result = await userManager.CreateAsync(user, model.Password);
        if (!result.Succeeded)
            throw new ProcessException($"Creating user account is wrong. {String.Join(", ", result.Errors.Select(s => s.Description))}");


        var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
        var codeBytes = System.Text.Encoding.UTF8.GetBytes(code);
        code = System.Convert.ToBase64String(codeBytes);

        var url = generalSettings.MainUrlApi.TrimEnd('/') + $"/api/v1/accounts/email/confirm?user={user.Id}&code={code}";
        await rabbitMqTask.SendEmail(new EmailModel()
        {
            Email = model.Email,
            Subject = "DSR Prictice",
            Message = $"Confirm your registration by clicking on the <a href='{url}'>link</a>"
        });

        return mapper.Map<UserAccountModel>(user);
    }

    public async Task ConfirmEmail(Guid userId, string code)
    {
        var user = await userManager.FindByIdAsync(userId.ToString());
        if (user == null)
            throw new ProcessException("User does not exist");

        var codeBase64 = System.Convert.FromBase64String(code);
        code = System.Text.Encoding.UTF8.GetString(codeBase64);

        var res = await userManager.ConfirmEmailAsync(user, code);
        if (!res.Succeeded)
            throw new ProcessException("Failed to verify email");
    }

    public async Task<bool> CheckConfirmEmail(string email)
    {
        var user = await userManager.FindByEmailAsync(email);
        if (user == null)
            throw new ProcessException("User does not exist");
        return user.EmailConfirmed;
    }
    public async Task ResetPassword(ResetPasswordUserModel model)
    {
        var user = await userManager.FindByEmailAsync(model.Email);
        if (user == null)
            throw new ProcessException($"The user account with the email address {model.Email} does not exist.");

        var code = await userManager.GeneratePasswordResetTokenAsync(user);
        var codeBytes = System.Text.Encoding.UTF8.GetBytes(code);
        code = System.Convert.ToBase64String(codeBytes);

        var url = generalSettings.MainUrlApi.TrimEnd('/') + $"/api/v1/accounts/resetpassword/confirm?user={user.Id}&code={code}&password={model.Password}";

        await rabbitMqTask.SendEmail(new EmailModel()
        {
            Email = model.Email,
            Subject = "DSR Prictice",
            Message = $"Confirm password reset by following this <a href='{url}'>link</a>"
        });
    }

    public async Task ConfirmResetPassword(Guid userId, string code, string password)
    {
        var user = await userManager.FindByIdAsync(userId.ToString());
        if (user == null)
            throw new ProcessException("User does not exist");

        var codeBase64 = System.Convert.FromBase64String(code);
        code = System.Text.Encoding.UTF8.GetString(codeBase64);

        var res = await userManager.ResetPasswordAsync(user, code, password);
        if (!res.Succeeded)
            throw new ProcessException("Failed to confirm password change");
    }

    public async Task<UserAccountModel> GetUser(string token)
    {
        var user = await GetUserByToken(token);

        return mapper.Map<UserAccountModel>(user);
    }

    public async Task EditUserFullName(string token, string fullName)
    {
        if (string.IsNullOrEmpty(fullName) || fullName.Count() > 50 || string.IsNullOrWhiteSpace(fullName))
            throw new ProcessException("Invalid fullName!");
        var user = await GetUserByToken(token);

        user.FullName = fullName;

        await userManager.UpdateAsync(user);
    }

    public async Task EditUserEmail(string token, string email)
    {
        if (string.IsNullOrEmpty(email) || email.Count() > 50 || string.IsNullOrWhiteSpace(email))
            throw new ProcessException("Invalid email!");

        await FreeEmail(email);

        var user = await GetUserByToken(token);

        await userManager.SetEmailAsync(user, email);
        await userManager.SetUserNameAsync(user, email);

        var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
        var codeBytes = System.Text.Encoding.UTF8.GetBytes(code);
        code = System.Convert.ToBase64String(codeBytes);

        var url = generalSettings.MainUrlApi.TrimEnd('/') + $"/api/v1/accounts/email/confirm?user={user.Id}&code={code}";
        await rabbitMqTask.SendEmail(new EmailModel()
        {
            Email = email,
            Subject = "DSR Prictice",
            Message = $"To confirm your email follow this <a href='{url}'>link</a>"
        });
    }

    public async Task ChangePassword(string token, ChangePasswordModel model)
    {
        var user = await GetUserByToken(token);
        var res = await userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
        if (!res.Succeeded)
            throw new ProcessException("Wrong old password");
    }

    private async Task<User> GetUserByToken(string token)
    {
        var handler = new JwtSecurityTokenHandler();
        var jsonToken = handler.ReadToken(token);
        var tokenS = jsonToken as JwtSecurityToken;

        var idUser = tokenS.Claims.First(claim => claim.Type == "sub").Value;

        var user = await userManager.FindByIdAsync(idUser);
        if (user == null)
            throw new ProcessException("User is not found");

        return user;
    }

    public async Task<DateTime> LifetimeAccessToken(string token)
    {
        try
        {
            var dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token);
            var tokenS = jsonToken as JwtSecurityToken;

            var dateTimeOffset = tokenS.Claims.First(claim => claim.Type == "exp").Value;    
            
            return dateTime.AddSeconds(int.Parse(dateTimeOffset)).ToLocalTime();

        }
        catch (Exception ex)
        {
            return DateTime.MinValue;
        }
    }

    private async Task FreeEmail(string email)
    {
        var user = await userManager.FindByEmailAsync(email);
        if (user != null)
            throw new ProcessException($"User account with email {email} already exist.");
    }
}
