﻿using AutoMapper;
using DSR.Db.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.AutoServices.Marks.Models
{
    public class UpdateMarkModel
    {
        public string Title { get; set; } = string.Empty;
        public int ModelId { get; set; }
    }
    public class UpdateMarkModelValidator : AbstractValidator<UpdateMarkModel>
    {
        public UpdateMarkModelValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");
            RuleFor(x => x.ModelId)
                .NotEmpty().WithMessage("Modelid is required");
        }
    }
    public class UpdateMarkModelProfile : Profile
    {
        public UpdateMarkModelProfile()
        {
            CreateMap<UpdateMarkModel, Mark>();
        }
    }
}
