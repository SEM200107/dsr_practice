﻿using AutoMapper;
using DSR.Db.Entities;

namespace DSR.Services.Marks.Model
{
    public class MarkModel
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public int ModelId { get; set; }
        public string Model { get; set; } = string.Empty;
    }
    public class MarkModelProfile : Profile
    {
        public MarkModelProfile()
        {
            CreateMap<Mark, MarkModel>()
                .ForMember(dest => dest.Model, opt => opt.MapFrom(src => src.Model.Title));
        }
    }
}
