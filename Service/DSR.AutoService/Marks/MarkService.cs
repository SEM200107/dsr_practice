﻿using AutoMapper;
using DSR.Api.Controllers.Auto;
using DSR.AutoServices.Marks.Models;
using DSR.Db.Context.Context;
using DSR.Db.Entities;
using DSR.Services.Marks.Model;
using DSR.Shared.Common.Extensions;
using DSR.Shared.Common.Validator;
using Microsoft.EntityFrameworkCore;

namespace DSR.Services.Marks
{
    public class MarkService : IMarkService
    {
        private readonly IDbContextFactory<MainDbContext> contextFactory;
        private readonly IMapper mapper;
        private readonly IModelValidator<AddMarkModel> addMarkModelValidator;
        private readonly IModelValidator<UpdateMarkModel> updateMarkModelValidator;

        public MarkService(IDbContextFactory<MainDbContext> contextFactory, IMapper mapper, IModelValidator<AddMarkModel> addMarkModelValidator, IModelValidator<UpdateMarkModel> updateMarkModelValidator)
        {
            this.contextFactory = contextFactory;
            this.mapper = mapper;
            this.addMarkModelValidator = addMarkModelValidator;
            this.updateMarkModelValidator = updateMarkModelValidator;
        }
        public async Task<IEnumerable<MarkModel>> GetMarks()
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var marks = context.Marks
                .Include(x => x.Model)
                .AsQueryable();            

            var data = (await marks.ToListAsync()).Select(mark => mapper.Map<MarkModel>(mark));

            return data;
        }
        public async Task<MarkModel> GetMark(int id)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var mark = await context.Marks
                .Include(x => x.Model)
                .FirstOrDefaultAsync(mark => mark.Id.Equals(id));

            var data = mapper.Map<MarkModel>(mark);

            return data;
        }
        public async Task<MarkModel> AddMark(AddMarkModel model)
        {
            addMarkModelValidator.Check(model);

            using var context = await contextFactory.CreateDbContextAsync();

            var mark = mapper.Map<Mark>(model);
            await context.Marks.AddAsync(mark);
            context.SaveChanges();

            return mapper.Map<MarkModel>(mark);
        }
        public async Task UpdateMark(int id, UpdateMarkModel model)
        {
            updateMarkModelValidator.Check(model);

            using var context = await contextFactory.CreateDbContextAsync();

            var mark = await context.Marks.FirstOrDefaultAsync(x => x.Id.Equals(id));
            ProcessException.ThrowIf(() => mark is null, $"The mark (id: {id}) was not found");
            mark = mapper.Map(model, mark);

            context.Marks.Update(mark);
            context.SaveChanges();
        }
        public async Task DeleteMark(int id)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var mark = await context.Marks.FirstOrDefaultAsync(x => x.Id.Equals(id));
            ProcessException.ThrowIf(() => mark is null, $"The mark (id: {id}) was not found");

            context.Remove(mark);
            context.SaveChanges();
        }
    }
}
