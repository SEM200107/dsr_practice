﻿using DSR.AutoServices.Marks.Models;
using DSR.Services.Marks.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.Services.Marks
{
    public interface IMarkService
    {
        Task<IEnumerable<MarkModel>> GetMarks();
        Task<MarkModel> GetMark(int id);
        Task<MarkModel> AddMark(AddMarkModel model);
        Task UpdateMark(int id, UpdateMarkModel model);
        Task DeleteMark(int id);
    }
}
