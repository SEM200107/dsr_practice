﻿namespace DSR.AutoService;

using DSR.AutoServices.Comments;
using DSR.Services.Calors;
using DSR.Services.Marks;
using DSR.Services.Models;
using Microsoft.Extensions.DependencyInjection;

public static class Bootstrapper
{
    public static IServiceCollection AddAutoService(this IServiceCollection services)
    {
        services.AddSingleton<IAutoService, AutoService>();
        services.AddSingleton<IColorService, ColorService>();
        services.AddSingleton<IMarkService, MarkService>();
        services.AddSingleton<IModelService, ModelService>();
        services.AddSingleton<ICommentService, CommentService>();

        return services;
    }
}
