﻿using AutoMapper;
using DSR.Db.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.AutoServices.Comments.Models
{
    public class AddCommentModel
    {
        public string Content { get; set; }
        public Guid UserId { get; set; }
        public int AutomobileId { get; set; }
    }
    public class AddCommentModelValidator : AbstractValidator<AddCommentModel>
    {
        public AddCommentModelValidator()
        {
            RuleFor(x => x.Content)
                .NotEmpty().WithMessage("Content is required")
                .MaximumLength(500).WithMessage("Content is long");
            RuleFor(x => x.UserId)
                .NotEmpty().WithMessage("User is required");
            RuleFor(x => x.AutomobileId)
                .NotEmpty().WithMessage("Automobile is required");
        }
    }
    public class AddCommentModelProfile : Profile
    {
        public AddCommentModelProfile()
        {
            CreateMap<AddCommentModel, Comment>();
        }
    }
}
