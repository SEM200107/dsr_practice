﻿using AutoMapper;
using DSR.Db.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.AutoServices.Comments.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public Guid UserId { get; set; }
        public string FullNameUser { get; set; }
        public int AutomobileId { get; set; }
    }
    public class CommentModelProfile : Profile
    {
        public CommentModelProfile()
        {
            CreateMap<Comment, CommentModel>()
                .ForMember(dest => dest.FullNameUser, opt => opt.MapFrom(src => src.User.FullName));
        }
    }
}
