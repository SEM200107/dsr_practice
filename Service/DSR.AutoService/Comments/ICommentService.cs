﻿using DSR.AutoServices.Comments.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.AutoServices.Comments
{
    public interface ICommentService
    {
        Task<IEnumerable<CommentModel>> GetCommentsByAutoId(int idAuto);
        Task<CommentModel> GetCommentById(int id);
        Task<CommentModel> AddComment(AddCommentModel model);
        Task UpdateComment(int id, UpdateCommentModel model);
        Task DeleteComment(int id);

    }
}
