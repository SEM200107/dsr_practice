﻿using AutoMapper;
using DSR.AutoServices.Comments.Models;
using DSR.Db.Context.Context;
using DSR.Db.Entities;
using DSR.Shared.Common.Extensions;
using DSR.Shared.Common.Validator;
using Microsoft.EntityFrameworkCore;

namespace DSR.AutoServices.Comments
{
    public class CommentService : ICommentService
    {
        private readonly IDbContextFactory<MainDbContext> contextFactory;
        private readonly IMapper mapper;
        private readonly IModelValidator<AddCommentModel> addCommentModelValidator;
        private readonly IModelValidator<UpdateCommentModel> updateCommentModelValidator;

        public CommentService(IDbContextFactory<MainDbContext> contextFactory, IMapper mapper, IModelValidator<AddCommentModel> addCommentModelValidator, IModelValidator<UpdateCommentModel> updateCommentModelValidator)
        {
            this.contextFactory = contextFactory;
            this.mapper = mapper;
            this.addCommentModelValidator = addCommentModelValidator;
            this.updateCommentModelValidator = updateCommentModelValidator;
        }
        public async Task<IEnumerable<CommentModel>> GetCommentsByAutoId(int idAuto)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var comments = context.Comments
                .Include(x => x.User)
                .AsQueryable();            
            
            var data = (await comments.ToListAsync()).Where(comment => comment.AutomobileId == idAuto);

            return mapper.Map<IEnumerable<CommentModel>>(data);
        }
        public async Task<CommentModel> GetCommentById(int id)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var comment = await context.Comments
                .Include(x => x.User)
                .FirstOrDefaultAsync(comment => comment.Id.Equals(id));

            var data = mapper.Map<CommentModel>(comment);

            return data;
        }
        public async Task<CommentModel> AddComment(AddCommentModel model)
        {
            addCommentModelValidator.Check(model);

            using var context = await contextFactory.CreateDbContextAsync();

            var comment = mapper.Map<Comment>(model);
            await context.Comments.AddAsync(comment);
            context.SaveChanges();

            return mapper.Map<CommentModel>(comment);
        }
        public async Task UpdateComment(int id, UpdateCommentModel model)
        {
            updateCommentModelValidator.Check(model);

            using var context = await contextFactory.CreateDbContextAsync();

            var comment = await context.Comments.FirstOrDefaultAsync(x => x.Id.Equals(id));
            ProcessException.ThrowIf(() => comment is null, $"The comment (id: {id}) was not found");
            comment = mapper.Map(model, comment);

            context.Comments.Update(comment);
            context.SaveChanges();
        }
        public async Task DeleteComment(int id)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var comment = await context.Comments.FirstOrDefaultAsync(x => x.Id.Equals(id));
            ProcessException.ThrowIf(() => comment is null, $"The comment (id: {id}) was not found");

            context.Remove(comment);
            context.SaveChanges();
        }
    }
}
