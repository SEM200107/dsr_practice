﻿using AutoMapper;
using DSR.AutoServices.Colors.Models;
using DSR.Db.Context.Context;
using DSR.Db.Entities;
using DSR.Services.Calors.Model;
using DSR.Shared.Common.Extensions;
using DSR.Shared.Common.Validator;
using Microsoft.EntityFrameworkCore;

namespace DSR.Services.Calors
{
    public class ColorService : IColorService
    {
        private readonly IDbContextFactory<MainDbContext> contextFactory;
        private readonly IMapper mapper;
        private readonly IModelValidator<AddColorModel> addColorModelValidator;
        private readonly IModelValidator<UpdateColorModel> updateColorModelValidator;

        public ColorService(IDbContextFactory<MainDbContext> contextFactory, IMapper mapper, IModelValidator<AddColorModel> addColorModelValidator, IModelValidator<UpdateColorModel> updateColorModelValidator)
        {
            this.contextFactory = contextFactory;
            this.mapper = mapper;
            this.addColorModelValidator = addColorModelValidator;
            this.updateColorModelValidator = updateColorModelValidator;
        }
        public async Task<IEnumerable<ColorModel>> GetColors()
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var colors = context.Colors.AsQueryable();            

            var data = (await colors.ToListAsync()).Select(color => mapper.Map<ColorModel>(color));

            return data;
        }
        public async Task<ColorModel> GetColor(int id)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var color = await context.Colors.FirstOrDefaultAsync(color => color.Id.Equals(id));

            var data = mapper.Map<ColorModel>(color);

            return data;
        }
        public async Task<ColorModel> AddColor(AddColorModel model)
        {
            addColorModelValidator.Check(model);

            using var context = await contextFactory.CreateDbContextAsync();

            var color = mapper.Map<Color>(model);
            await context.Colors.AddAsync(color);
            context.SaveChanges();

            return mapper.Map<ColorModel>(color);

        }
        public async Task UpdateColor(int id, UpdateColorModel model)
        {
            updateColorModelValidator.Check(model);

            using var context = await contextFactory.CreateDbContextAsync();

            var color = await context.Colors.FirstOrDefaultAsync(x => x.Id.Equals(id));
            ProcessException.ThrowIf(() => color is null, $"The color (id: {id}) was not found");
            color = mapper.Map(model, color);

            context.Colors.Update(color);
            context.SaveChanges();
        }
        public async Task DeleteColor(int id)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var color = await context.Colors.FirstOrDefaultAsync(x => x.Id.Equals(id));
            ProcessException.ThrowIf(() => color is null, $"The color (id: {id}) was not found");

            context.Remove(color);
            context.SaveChanges();
        }
    }
}
