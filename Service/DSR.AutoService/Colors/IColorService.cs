﻿using DSR.AutoServices.Colors.Models;
using DSR.Services.Calors.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.Services.Calors
{
    public interface IColorService
    {
        Task<IEnumerable<ColorModel>> GetColors();
        Task<ColorModel> GetColor(int id);
        Task<ColorModel> AddColor(AddColorModel model);
        Task UpdateColor(int id, UpdateColorModel model);
        Task DeleteColor(int id);

    }
}
