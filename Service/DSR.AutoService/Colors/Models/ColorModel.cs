﻿using AutoMapper;
using DSR.Db.Entities;

namespace DSR.Services.Calors.Model
{
    public class ColorModel
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
    }
    public class CalorModelProfile : Profile
    {
        public CalorModelProfile()
        {
            CreateMap<Color, ColorModel>();
        }
    }
}
