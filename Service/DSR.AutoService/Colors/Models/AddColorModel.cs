﻿using AutoMapper;
using DSR.Db.Entities;
using FluentValidation;

namespace DSR.AutoServices.Colors.Models
{
    public class AddColorModel
    {
        public string Title { get; set; } = string.Empty;
    }
    public class AddColorModelValidator : AbstractValidator<AddColorModel>
    {
        public AddColorModelValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");
        }
    }
    public class AddColorModelProfile : Profile
    {
        public AddColorModelProfile()
        {
            CreateMap<AddColorModel, Color>();
        }
    }
}
