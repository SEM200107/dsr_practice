﻿using AutoMapper;
using DSR.Db.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.AutoServices.Colors.Models
{
    public class UpdateColorModel
    {
        public string Title { get; set; } = string.Empty;
    }
    public class UpdateColorModelValidator : AbstractValidator<UpdateColorModel>
    {
        public UpdateColorModelValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");
        }
    }
    public class UpdateColorModelProfile : Profile
    {
        public UpdateColorModelProfile()
        {
            CreateMap<UpdateColorModel, Color>();
        }
    }
}
