﻿using AutoMapper;
using DSR.Db.Entities;
using FluentValidation;

namespace DSR.Api.Controllers.Auto
{
    public class UpdateAutoModel
    {
        public string Number { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public int ColorId { get; set; }
        public int MarkId { get; set; }
    }
    
    public class UpdateAutoModelValidator : AbstractValidator<UpdateAutoModel>
    {
        public UpdateAutoModelValidator()
        {
            RuleFor(x => x.Number)
                .NotEmpty().WithMessage("Number is required")
                .MaximumLength(9).WithMessage("Number is long");
            RuleFor(x => x.ColorId)
                .NotEmpty().WithMessage("Calor is required");
            RuleFor(x => x.Description)
                .NotEmpty().WithMessage("Description is required")
                .MaximumLength(1000).WithMessage("Description is long");
            RuleFor(x => x.MarkId)
                .NotEmpty().WithMessage("Mark is required");
        }
    }
    public class UpdateAutoModelProfile : Profile
    {
        public UpdateAutoModelProfile()
        {
            CreateMap<UpdateAutoModel, Automobile>();
        }
    }
}
