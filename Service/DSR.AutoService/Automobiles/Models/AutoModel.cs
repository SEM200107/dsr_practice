﻿using AutoMapper;
using DSR.Db.Entities;

namespace DSR.Api.Controllers.Auto
{
    public class AutoModel
    {
        public int Id { get; set; }
        public string Number { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Color { get; set; } = string.Empty;
        public int ColorId { get; set; }
        public string Model { get; set; } = string.Empty;
        public int ModelId { get; set; }
        public string Mark { get; set; } = string.Empty;
        public int MarkId { get; set; }
    }
    public class AutoModelProfile : Profile
    {
        public AutoModelProfile()
        {
            CreateMap<Automobile, AutoModel>()
                .ForMember(dest => dest.Color, opt => opt.MapFrom(src => src.Color.Title))
                .ForMember(dest => dest.Model, opt => opt.MapFrom(src => src.Mark.Model.Title))
                .ForMember(dest => dest.ModelId, opt => opt.MapFrom(src => src.Mark.ModelId))
                .ForMember(dest => dest.Mark, opt => opt.MapFrom(src => src.Mark.Title));
        }
    }
}
