﻿using DSR.Api.Controllers.Auto;
using DSR.Db.Entities;

namespace DSR.AutoService
{
    public interface IAutoService
    {
        Task<IEnumerable<AutoModel>> GetAutos(int offset = 0, int limit = 10);
        Task<AutoModel> GetAuto(int id);
        Task<AutoModel> AddAuto(AddAutoModel model);
        Task UpdateAuto(int id, UpdateAutoModel model);
        Task DeleteAuto(int id);
    }
}
