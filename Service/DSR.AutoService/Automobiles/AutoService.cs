﻿using AutoMapper;
using DSR.Api.Controllers.Auto;
using DSR.Db.Context.Context;
using DSR.Db.Entities;
using DSR.Shared.Common.Extensions;
using DSR.Shared.Common.Validator;
using Microsoft.EntityFrameworkCore;

namespace DSR.AutoService
{
    public class AutoService : IAutoService
    {
        private readonly IDbContextFactory<MainDbContext> contextFactory;
        private readonly IMapper mapper;
        private readonly IModelValidator<AddAutoModel> addAutoModelValidator;
        private readonly IModelValidator<UpdateAutoModel> updateAutoModelValidator;

        public AutoService(IDbContextFactory<MainDbContext> contextFactory, IMapper mapper, IModelValidator<AddAutoModel> addAutoModelValidator, IModelValidator<UpdateAutoModel> updateAutoModelValidator)
        {
            this.contextFactory = contextFactory;
            this.mapper = mapper;
            this.addAutoModelValidator = addAutoModelValidator;
            this.updateAutoModelValidator = updateAutoModelValidator;
        }
        public async Task<IEnumerable<AutoModel>> GetAutos(int offset = 0, int limit = 10)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var autos = context.Automobiles
                .Include(x => x.Color)
                .Include(x => x.Mark.Model)
                .Include(x => x.Mark)
                .AsQueryable();

            autos = autos
                .Skip(Math.Max(offset, 0))
                .Take(Math.Max(0, Math.Min(limit, 1000)));

            var data = (await autos.ToListAsync()).Select(auto => mapper.Map<AutoModel>(auto));

            return data;
        }
        public async Task<AutoModel> GetAuto(int id)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var auto = await context.Automobiles
                .Include(x => x.Color)
                .Include(x => x.Mark.Model)
                .Include(x => x.Mark)
                .FirstOrDefaultAsync(auto => auto.Id.Equals(id));

            var data = mapper.Map<AutoModel>(auto);

            return data;
        }
        public async Task<AutoModel> AddAuto(AddAutoModel model)
        {
            addAutoModelValidator.Check(model);

            using var context = await contextFactory.CreateDbContextAsync();

            var auto = mapper.Map<Automobile>(model);
            await context.Automobiles.AddAsync(auto);
            context.SaveChanges();

            return mapper.Map<AutoModel>(auto);
        }
        public async Task UpdateAuto(int id, UpdateAutoModel model)
        {
            updateAutoModelValidator.Check(model);

            using var context = await contextFactory.CreateDbContextAsync();

            var auto = await context.Automobiles.FirstOrDefaultAsync(x => x.Id.Equals(id));
            ProcessException.ThrowIf(() => auto is null, $"The auto (id: {id}) was not found");
            auto = mapper.Map(model, auto);

            context.Automobiles.Update(auto);
            context.SaveChanges();
        }
        public async Task DeleteAuto(int id)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var auto = await context.Automobiles.FirstOrDefaultAsync(x => x.Id.Equals(id));
            ProcessException.ThrowIf(() => auto is null, $"The auto (id: {id}) was not found");

            context.Remove(auto);
            context.SaveChanges();
        }
    }
}
