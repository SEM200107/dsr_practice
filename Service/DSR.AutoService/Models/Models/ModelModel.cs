﻿using AutoMapper;
using DSR.Db.Entities;

namespace DSR.Services.Models.Model
{
    public class ModelModel
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
    }
    public class ModelModelProfile : Profile
    {
        public ModelModelProfile()
        {
            CreateMap<DSR.Db.Entities.Model, ModelModel>();
        }
    }
}
