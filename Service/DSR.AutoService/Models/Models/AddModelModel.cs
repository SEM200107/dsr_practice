﻿using AutoMapper;
using DSR.Db.Entities;
using FluentValidation;

namespace DSR.Services.Models.Model
{
    public class AddModelModel
    {
        public string Title { get; set; } = string.Empty;
    }
    public class AddModelModelValidator : AbstractValidator<AddModelModel>
    {
        public AddModelModelValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");
        }
    }
    public class AddModelModelProfile : Profile
    {
        public AddModelModelProfile()
        {
            CreateMap<AddModelModel, DSR.Db.Entities.Model>();
        }
    }
}
