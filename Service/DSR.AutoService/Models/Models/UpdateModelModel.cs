﻿using AutoMapper;
using DSR.Db.Entities;
using FluentValidation;

namespace DSR.Services.Models.Model
{
    public class UpdateModelModel
    {
        public string Title { get; set; } = string.Empty;
    }
    public class UpdateModelModelValidator : AbstractValidator<UpdateModelModel>
    {
        public UpdateModelModelValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title is required")
                .MaximumLength(30).WithMessage("Title is long");
        }
    }
    public class UpdateModelModelProfile : Profile
    {
        public UpdateModelModelProfile()
        {
            CreateMap<UpdateModelModel, DSR.Db.Entities.Model>();
        }
    }
}
