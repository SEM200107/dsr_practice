﻿using AutoMapper;
using DSR.Db.Context.Context;
using DSR.Services.Models.Model;
using DSR.Shared.Common.Extensions;
using DSR.Shared.Common.Validator;
using Microsoft.EntityFrameworkCore;

namespace DSR.Services.Models
{
    public class ModelService : IModelService
    {
        private readonly IDbContextFactory<MainDbContext> contextFactory;
        private readonly IMapper mapper;
        private readonly IModelValidator<AddModelModel> addModelModelValidator;
        private readonly IModelValidator<UpdateModelModel> updateModelModelValidator;

        public ModelService(IDbContextFactory<MainDbContext> contextFactory, IMapper mapper, IModelValidator<AddModelModel> addModelModelValidator, IModelValidator<UpdateModelModel> updateModelModelValidator)
        {
            this.contextFactory = contextFactory;
            this.mapper = mapper;
            this.addModelModelValidator = addModelModelValidator;
            this.updateModelModelValidator = updateModelModelValidator;
        }
        public async Task<IEnumerable<ModelModel>> GetModels()
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var modelsAuto = context.Models.AsQueryable();           

            var data = (await modelsAuto.ToListAsync()).Select(model => mapper.Map<ModelModel>(model));

            return data;
        }
        public async Task<ModelModel> GetModel(int id)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var modelAuto = await context.Models.FirstOrDefaultAsync(model => model.Id.Equals(id));

            var data = mapper.Map<ModelModel>(modelAuto);

            return data;
        }
        public async Task<ModelModel> AddModel(AddModelModel model)
        {
            addModelModelValidator.Check(model);

            using var context = await contextFactory.CreateDbContextAsync();

            var modelAuto = mapper.Map<DSR.Db.Entities.Model>(model);
            await context.Models.AddAsync(modelAuto);
            context.SaveChanges();

            return mapper.Map<ModelModel>(modelAuto);

        }
        public async Task UpdateModel(int id, UpdateModelModel model)
        {
            updateModelModelValidator.Check(model);

            using var context = await contextFactory.CreateDbContextAsync();

            var modelAuto = await context.Models.FirstOrDefaultAsync(x => x.Id.Equals(id));
            ProcessException.ThrowIf(() => modelAuto is null, $"The model (id: {id}) was not found");
            modelAuto = mapper.Map(model, modelAuto);

            context.Models.Update(modelAuto);
            context.SaveChanges();
        }
        public async Task DeleteModel(int id)
        {
            using var context = await contextFactory.CreateDbContextAsync();

            var modelAuto = await context.Models.FirstOrDefaultAsync(x => x.Id.Equals(id));
            ProcessException.ThrowIf(() => modelAuto is null, $"The model (id: {id}) was not found");

            context.Remove(modelAuto);
            context.SaveChanges();
        }
    }
}
