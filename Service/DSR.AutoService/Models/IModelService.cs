﻿using DSR.Services.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.Services.Models
{
    public interface IModelService
    {
        Task<IEnumerable<ModelModel>> GetModels();
        Task<ModelModel> GetModel(int id);
        Task<ModelModel> AddModel(AddModelModel model);
        Task UpdateModel(int id, UpdateModelModel model);
        Task DeleteModel(int id);
    }
}
