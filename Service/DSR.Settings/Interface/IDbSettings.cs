﻿namespace DSR.Settings;

public interface IDbSettings
{
    string ConnectionString{ get; }
}
