﻿namespace DSR.Settings;

public interface IGeneralSettings
{
    string MainUrlApi { get; }

    string MainUrlWeb { get; }  

    bool SwaggerVisible { get; }
}
