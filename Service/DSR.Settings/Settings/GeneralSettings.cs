﻿namespace DSR.Settings;

public class GeneralSettings : IGeneralSettings
{
    private readonly ISettingsSource source;
    public GeneralSettings(ISettingsSource source) => this.source = source;

    public string MainUrlApi => source.GetAsString("General:MainUrlApi");

    public string MainUrlWeb => source.GetAsString("General:MainUrlWeb");

    public bool SwaggerVisible => source.GetAsBool("General:SwaggerVisible");
}
