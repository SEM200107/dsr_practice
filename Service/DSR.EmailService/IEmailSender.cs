namespace DSR.EmailService;

using DSR.RabbitMQService;

public interface IEmailSender
{
    Task SendEmailAsync(EmailModel model);
    Task SendEmailAsync(string email, string subject, string message);
}
