﻿using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class AccountIntegrationTest
    {
        const string OldPassword = "test";

        [Test]
        public async Task ChangePasswordUser_ValidParameters_OkResponse()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.ChangePassword(accessToken);

            var request = ChangePasswordRequest(OldPassword, Generator.ValidPasswords.First());
            var response = await apiClient.PostJson(url, request, accessToken);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidPasswords))]
        public async Task ChangePasswordUser_InvalidNewPassword_BadRequest(string password)
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.ChangePassword(accessToken);

            var request = ChangePasswordRequest(OldPassword, password);
            var response = await apiClient.PostJson(url, request, accessToken);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task ChangePasswordUser_InvalidOldPassword_BadRequest()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.ChangePassword(accessToken);

            var request = ChangePasswordRequest(OldPassword+"123", Generator.ValidPasswords.First());
            var response = await apiClient.PostJson(url, request, accessToken);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task ChangePasswordUser_Unauthorized()
        {
            await CheckTestUser();
            var url = Urls.ChangePassword(await AuthenticateUser_ReadAndWriteAutosScope());
            var request = ChangePasswordRequest(OldPassword, Generator.ValidPasswords.First());
            var response = await apiClient.PostJson(url, request, null);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task ChangePasswordUser_EmptyScope_Forbidden()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.ChangePassword(await AuthenticateUser_ReadAndWriteAutosScope());
            var request = ChangePasswordRequest(OldPassword, Generator.ValidPasswords.First());
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
