﻿using DSR.Api.Controllers.Accounts.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class AccountIntegrationTest
    {
        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.ValidNames))]
        public async Task EditUserFullName_ValidParameters_Authenticated_OkResponse(string name)
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.EditUserFullName(accessToken, name);

            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async Task EditUserFullName_InvalidToken_Authenticated_BadRequest()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.EditUserFullName("InvalidAccessToken", Generator.ValidNames.First());

            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidNames))]
        public async Task EditUserFullName_InvalidName_Authenticated_BadRequest(string name)
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.EditUserFullName(accessToken, name);

            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task EditUserFullName_Unauthorized()
        {
            await CheckTestUser();
            var url = Urls.EditUserFullName(await AuthenticateUser_ReadAndWriteAutosScope(), Generator.ValidNames.First());
            var response = await apiClient.Get(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task EditUserFullName_EmptyScope_Forbidden()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.EditUserFullName(await AuthenticateUser_ReadAndWriteAutosScope(), Generator.ValidNames.First());
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
