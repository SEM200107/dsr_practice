﻿using DSR.Api.Test.Common;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSR.Db.Entities;
using DSR.Shared.Common.Security;
using System;
using DSR.Db.Entities.User;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    [TestFixture]
    public partial class AccountIntegrationTest : ComponentTest
    {
        const string EmailTestUser = "test@test.ru";
        const string PasswordTestUser = "test";

        [SetUp]
        public async Task SetUp()
        {
            await using var context = await DbContext();
            context.Users.RemoveRange(context.Users);

            var user = new User()
            {
                Status = UserStatus.Active,
                FullName = EmailTestUser,
                UserName = EmailTestUser,
                Email = EmailTestUser,
                EmailConfirmed = true,
                PhoneNumber = null,
                PhoneNumberConfirmed = false
            };
            await userManager.CreateAsync(user, PasswordTestUser);

            context.SaveChanges();
        }

        [TearDown]
        public async override Task TearDown()
        {
            await using var context = await DbContext();
            context.Users.RemoveRange(context.Users);
            context.SaveChanges();
            await base.TearDown();
        }

        protected static class Urls
        {
            public static string Register => $"/api/v1/accounts";

            public static string GetUser(string token) => $"/api/v1/accounts/profile/{token}";

            public static string EditUserFullName(string token, string fullName) => $"/api/v1/accounts/profile/editfullname/{token}/{fullName}";

            public static string EditUserEmail(string token, string email) => $"/api/v1/accounts/profile/editemail/{token}/{email}";

            public static string ChangePassword(string token) => $"/api/v1/accounts/changepassword/{token}";


        }

        public static class Scopes
        {
            public static string ReadAutos => $"offline_access {AppScopes.AutomobilesRead}";

            public static string WriteAutos => $"offline_access {AppScopes.AutomobilesWrite}";

            public static string ReadAndWriteAutos => $"offline_access {AppScopes.AutomobilesRead} {AppScopes.AutomobilesWrite}";

            public static string Empty => "offline_access";
        }

        public async Task<string> AuthenticateUser_ReadAndWriteAutosScope()
        {
            var tokenResponse = await AuthenticateTestUser(EmailTestUser, PasswordTestUser, Scopes.ReadAndWriteAutos);
            return tokenResponse.AccessToken;
        }

        public async Task<string> AuthenticateUser_EmptyScope()
        {
            var tokenResponse = await AuthenticateTestUser(EmailTestUser, PasswordTestUser, Scopes.Empty);
            return tokenResponse.AccessToken;
        }

        public async Task CheckTestUser()
        {
            var user1 = userManager.FindByEmailAsync(EmailTestUser);
            if (!user1.IsCompleted)
            {
                var user = new User()
                {
                    Status = UserStatus.Active,
                    FullName = EmailTestUser,
                    UserName = EmailTestUser,
                    Email = EmailTestUser,
                    EmailConfirmed = true,
                    PhoneNumber = null,
                    PhoneNumberConfirmed = false
                };
                await userManager.CreateAsync(user, PasswordTestUser);
            }
        }

        public async Task<Guid> GetExistedUserId()
        {
            return (await userManager.FindByEmailAsync(EmailTestUser)).Id;
        }

        public async Task<Guid> GetNotExistedGuidId()
        {
            return Guid.NewGuid();
        }
    }
}
