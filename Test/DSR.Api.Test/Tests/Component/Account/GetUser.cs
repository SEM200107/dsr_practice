﻿using DSR.Api.Controllers.Accounts.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class AccountIntegrationTest
    {
        [Test]
        public async Task GetUser_ValidParameters_Authenticated_OkResponse()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetUser(accessToken);

            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var user_from_api = await response.ReadAsObject<UserAccountResponse>();
            var user_from_db = await userManager.FindByEmailAsync("test@test.ru");

            Assert.AreEqual(user_from_db.Id, user_from_api.Id);
        }

        [Test]
        public async Task GetUser_InvalidToken_Authenticated_BadRequest()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetUser("InvalidAccessToken");

            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task GetUser_Unauthorized()
        {
            await CheckTestUser();
            var url = Urls.GetUser(await AuthenticateUser_ReadAndWriteAutosScope());
            var response = await apiClient.Get(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task GetUser_EmptyScope_Forbidden()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.GetUser(await AuthenticateUser_ReadAndWriteAutosScope());
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
