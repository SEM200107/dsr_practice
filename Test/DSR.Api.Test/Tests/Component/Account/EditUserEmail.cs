﻿using DSR.Api.Controllers.Accounts.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class AccountIntegrationTest
    {
        [Test]
        public async Task EditUserEmail_ValidParameters_Authenticated_OkResponse()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.EditUserEmail(accessToken, Generator.ValidEmails.First());

            var response = await apiClient.Get(url, accessToken);

            var userId = (await userManager.FindByEmailAsync(Generator.ValidEmails.First())).Id;
            await using var context = await DbContext();
            var newUser = context.Users.FirstOrDefault(x => x.Id == userId);

            Assert.IsNotNull(newUser);
            Assert.AreEqual(Generator.ValidEmails.First(), newUser?.Email);
        }

        [Test]
        public async Task EditUserEmail_InvalidToken_Authenticated_BadRequest()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.EditUserFullName("InvalidAccessToken", Generator.ValidEmails.First());

            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task EditUserEmail_InvalidName_Authenticated_BadRequest()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.EditUserFullName(accessToken, Generator.InvalidEmails.First());

            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task EditUserEmail_Unauthorized()
        {
            await CheckTestUser();
            var url = Urls.EditUserEmail(await AuthenticateUser_ReadAndWriteAutosScope(), Generator.ValidEmails.First());
            var response = await apiClient.Get(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task EditUserEmail_EmptyScope_Forbidden()
        {
            await CheckTestUser();
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.EditUserEmail(await AuthenticateUser_ReadAndWriteAutosScope(), Generator.ValidEmails.First());
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
