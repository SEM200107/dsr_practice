﻿namespace DSR.Api.Test.Tests.Component.Automobile;

public partial class AccountIntegrationTest
{
    public static class Generator
    {

        public static string[] ValidNames =
        {
            new string('1',4),
            new string('1',50)
        };

        public static string[] InvalidNames =
        {
            new string('1',51),
        };

        public static string[] ValidEmails =
        {
            new string("tester@mail.ru")
        };

        public static string[] InvalidEmails =
        {
            new string("11111111111111111111111111111111111111111111111111@mail.ru")
        };

        public static string[] ValidPasswords =
        {
            new string('1',8),
            new string('1',50)
        };

        public static string[] InvalidPasswords =
        {
            null,
            "",
            new string('1',51),
        };
    }
}
