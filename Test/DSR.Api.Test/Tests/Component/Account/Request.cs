﻿namespace DSR.Api.Test.Tests.Component.Automobile;

using DSR.Api.Controllers.Accounts.Models;
using DSR.Api.Controllers.Color.Models;

public partial class AccountIntegrationTest
{
    public static RegisterUserAccountRequest RegisterUserAccountRequest(string name, string email, string password)
    {
        return new RegisterUserAccountRequest()
        {
            Name = name,
            Email = email,
            Password = password
        };
    }

    public static ChangePasswordRequest ChangePasswordRequest(string oldPassword, string newPassword)
    {
        return new ChangePasswordRequest()
        {
            OldPassword = oldPassword,
            NewPassword = newPassword
        };
    }
}
