﻿namespace DSR.Api.Test.Tests.Component.Automobile;

public partial class MarkIntegrationTest
{
    public static class Generator
    {

        public static string[] ValidTitles =
        {
            new string('1',1),
            new string('1',30)
        };

        public static string[] InvalidTitles =
        {
            null,
            "",
            new string('1',31),
        };

        public static int[] InvalidModelIds =
        {
            0,
            -1,
            int.MaxValue
        };        
    }
}
