﻿using DSR.Api.Controllers.Mark.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class MarkIntegrationTest
    {
        [Test]
        public async Task GetMark_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetMark(await GetExistedMarkId());
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var marks_from_api = await response.ReadAsObject<MarkResponse>();

            Assert.AreEqual(await GetExistedMarkId(), marks_from_api.Id);
        }

        [Test]
        public async Task GetMark_NegativeParameters_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetMark(await GetNotExistedMarkId());
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [Test]
        public async Task GetMark_Unauthorized()
        {
            var url = Urls.GetMark(1);
            var response = await apiClient.Get(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task GetMark_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.GetMark(1);
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
