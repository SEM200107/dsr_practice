﻿namespace DSR.Api.Test.Tests.Component.Automobile;

using DSR.Api.Controllers.Auto;
using DSR.Api.Controllers.Mark.Models;

public partial class MarkIntegrationTest
{
    public static AddMarkRequest AddMarkRequest(string title, int modelId)
    {
        return new AddMarkRequest()
        {
            Title = title,
            ModelId = modelId
        };
    }

    public static UpdateMarkRequest UpdateMarkRequest(string title, int modelId)
    {
        return new UpdateMarkRequest()
        {
            Title = title,
            ModelId = modelId
        };
    }
}
