﻿using DSR.Api.Controllers.Mark.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class MarkIntegrationTest
    {
        [Test]
        public async Task GetMarks_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetMarks();
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var marks_from_api = await response.ReadAsObject<IEnumerable<MarkResponse>>();

            await using var context = await DbContext();
            var marks_from_db = context.Marks.AsEnumerable();

            Assert.AreEqual(marks_from_db.Count(), marks_from_api.Count());
        }

        [Test]
        public async Task GetMarks_Unauthorized()
        {
            var url = Urls.GetMarks();
            var response = await apiClient.Get(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task GetMarks_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.GetMarks();
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
