﻿using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class MarkIntegrationTest
    {
        [Test]
        public async Task AddMark_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddMark;

            var modelId = await GetExistedModelId();
            var request = AddMarkRequest(Generator.ValidTitles.First(), modelId);
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);


            await using var context = await DbContext();
            var newMark = context.Marks.AsEnumerable().OrderByDescending(x => x.Id).FirstOrDefault();
            Assert.IsNotNull(newMark);

            Assert.AreEqual(request.ModelId, newMark?.ModelId);
            Assert.AreEqual(request.Title, newMark?.Title);
        }

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidModelIds))]
        public async Task AddMark_InvalidModel_Authenticated_BadRequest(int modelId)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddMark;

            var request = AddMarkRequest(Generator.ValidTitles.First(), modelId);
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidTitles))]
        public async Task AddMark_InvalidTitle_Authenticated_BadRequest(string title)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddMark;

            var request = AddMarkRequest(title, await GetExistedModelId());
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [Test]
        public async Task AddMark_Unauthorized()
        {
            var url = Urls.AddMark;

            var modelId = await GetExistedMarkId();
            var request = AddMarkRequest(Generator.ValidTitles.First(), modelId);
            var response = await apiClient.PostJson(url, request, null);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task AddMark_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.AddMark;

            var modelId = await GetExistedMarkId();
            var request = AddMarkRequest(Generator.ValidTitles.First(), modelId);
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
