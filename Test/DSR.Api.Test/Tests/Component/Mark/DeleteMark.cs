﻿using DSR.Api.Controllers.Mark.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class MarkIntegrationTest
    {
        [Test]
        public async Task DeleteMark_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var idMark = await GetExistedMarkId();
            var url = Urls.DeleteMark(idMark);
            await using var context = await DbContext();

            var auto = await context.Automobiles.FirstOrDefaultAsync(x => x.MarkId == idMark);
            context.Automobiles.Remove(auto);
            context.SaveChanges();

            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            
            var newMark = await context.Marks.FirstOrDefaultAsync(x => x.Id.Equals(idMark));
            Assert.IsNull(newMark);
        }

        [Test]
        public async Task DeleteMark_NegativeParameters_BadRequest()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.DeleteMark(await GetNotExistedMarkId());
            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task DeleteMark_Unauthorized()
        {
            var url = Urls.DeleteMark(1);
            var response = await apiClient.Delete(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task DeleteMark_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.DeleteMark(1);
            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
