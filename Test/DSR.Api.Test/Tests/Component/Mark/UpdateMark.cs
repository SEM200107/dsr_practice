﻿using DSRNetSchool.Api.Test.Common.Extensions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class MarkIntegrationTest
    {
        [Test]
        public async Task UpdateMark_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var idMark = await GetExistedMarkId();
            var url = Urls.UpdateMark(idMark);

            var modelId = await GetExistedModelId();
            var request = UpdateMarkRequest(Generator.ValidTitles.First(), modelId);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);


            await using var context = await DbContext();
            var newMark = await context.Marks.FirstOrDefaultAsync(x => x.Id.Equals(idMark));
            Assert.IsNotNull(newMark);

            Assert.AreEqual(request.ModelId, newMark?.ModelId);
            Assert.AreEqual(request.Title, newMark?.Title);
        }

        [Test]
        public async Task UpdateMark_InvalidMarkId_Authenticated_BadRequest()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateMark(await GetNotExistedMarkId());

            var modelId = await GetExistedMarkId();
            var request = UpdateMarkRequest(Generator.ValidTitles.First(), modelId);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidModelIds))]
        public async Task UpdateMark_InvalidModel_Authenticated_BadRequest(int modelId)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateMark(await GetExistedMarkId());

            var request = AddMarkRequest(Generator.ValidTitles.First(), modelId);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidTitles))]
        public async Task UpdateMark_InvalidTitle_Authenticated_BadRequest(string title)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateMark(await GetExistedMarkId());

            var request = AddMarkRequest(title, await GetExistedModelId());
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task UpdateMark_Unauthorized()
        {
            var url = Urls.UpdateMark(await GetExistedMarkId());

            var modelId = await GetExistedMarkId();
            var request = AddMarkRequest(Generator.ValidTitles.First(), modelId);
            var response = await apiClient.PutJson(url, request, null);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task UpdateMark_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.UpdateMark(await GetExistedMarkId());

            var modelId = await GetExistedMarkId();
            var request = AddMarkRequest(Generator.ValidTitles.First(), modelId);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
