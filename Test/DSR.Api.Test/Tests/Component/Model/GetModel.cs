﻿using DSR.Api.Controllers.Model.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class ModelIntegrationTest
    {
        [Test]
        public async Task GetModel_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetModel(await GetExistedModelId());
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var models_from_api = await response.ReadAsObject<ModelResponse>();

            Assert.AreEqual(await GetExistedModelId(), models_from_api.Id);
        }

        [Test]
        public async Task GetModel_NegativeParameters_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetModel(await GetNotExistedModelId());
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [Test]
        public async Task GetModel_Unauthorized()
        {
            var url = Urls.GetModel(1);
            var response = await apiClient.Get(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task GetModel_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.GetModel(1);
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
