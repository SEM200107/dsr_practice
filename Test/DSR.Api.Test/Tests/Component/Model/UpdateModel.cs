﻿using DSRNetSchool.Api.Test.Common.Extensions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class ModelIntegrationTest
    {
        [Test]
        public async Task UpdateModel_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var idModel = await GetExistedModelId();
            var url = Urls.UpdateModel(idModel);

            var request = UpdateModelRequest(Generator.ValidTitles.First());
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);


            await using var context = await DbContext();
            var newModel = await context.Models.FirstOrDefaultAsync(x => x.Id.Equals(idModel));

            Assert.IsNotNull(newModel);
            Assert.AreEqual(request.Title, newModel?.Title);
        }

        [Test]
        public async Task UpdateModel_InvalidColorId_Authenticated_BadRequest()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateModel(await GetNotExistedModelId());

            var request = UpdateModelRequest(Generator.ValidTitles.First());
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidTitles))]
        public async Task UpdateModel_InvalidTitle_Authenticated_BadRequest(string title)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateModel(await GetExistedModelId());

            var request = UpdateModelRequest(title);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task UpdateModel_Unauthorized()
        {
            var url = Urls.UpdateModel(await GetExistedModelId());

            var request = UpdateModelRequest(Generator.ValidTitles.First());
            var response = await apiClient.PutJson(url, request, null);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task UpdateColor_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.UpdateModel(await GetExistedModelId());

            var request = UpdateModelRequest(Generator.ValidTitles.First());
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
