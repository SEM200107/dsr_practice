﻿using DSR.Api.Controllers.Model.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class ModelIntegrationTest
    {
        [Test]
        public async Task GetModels_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetModels();
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var models_from_api = await response.ReadAsObject<IEnumerable<ModelResponse>>();

            await using var context = await DbContext();
            var models_from_db = context.Models.AsEnumerable();

            Assert.AreEqual(models_from_db.Count(), models_from_api.Count());
        }

        [Test]
        public async Task GetModels_Unauthorized()
        {
            var url = Urls.GetModels();
            var response = await apiClient.Get(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task GetModels_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.GetModels();
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
