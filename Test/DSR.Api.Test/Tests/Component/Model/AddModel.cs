﻿using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class ModelIntegrationTest
    {
        [Test]
        public async Task AddModel_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddModel;

            var request = AddModelRequest(Generator.ValidTitles.First());
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);


            await using var context = await DbContext();
            var newModel = context.Models.AsEnumerable().OrderByDescending(x => x.Id).FirstOrDefault();

            Assert.IsNotNull(newModel);
            Assert.AreEqual(request.Title, newModel?.Title);
        }        

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidTitles))]
        public async Task AddModel_InvalidTitle_Authenticated_BadRequest(string title)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddModel;

            var request = AddModelRequest(title);
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [Test]
        public async Task AddModel_Unauthorized()
        {
            var url = Urls.AddModel;

            var request = AddModelRequest(Generator.ValidTitles.First());
            var response = await apiClient.PostJson(url, request, null);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task AddModel_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.AddModel;

            var request = AddModelRequest(Generator.ValidTitles.First());
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
