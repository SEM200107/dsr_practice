﻿using DSR.Api.Controllers.Mark.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class ModelIntegrationTest
    {
        [Test]
        public async Task DeleteModel_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var idModel = await GetExistedModelId();
            var url = Urls.DeleteModel(idModel);
            await using var context = await DbContext();

            var mark = await context.Marks.FirstOrDefaultAsync(x => x.ModelId == idModel);
            var auto = await context.Automobiles.FirstOrDefaultAsync(x => x.MarkId == mark.Id);            
            context.Automobiles.Remove(auto);
            context.Marks.Remove(mark);
            context.SaveChanges();

            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            
            var newModel = await context.Models.FirstOrDefaultAsync(x => x.Id.Equals(idModel));
            Assert.IsNull(newModel);
        }

        [Test]
        public async Task DeleteModel_NegativeParameters_BadRequest()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.DeleteModel(await GetNotExistedModelId());
            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task DeleteModel_Unauthorized()
        {
            var url = Urls.DeleteModel(1);
            var response = await apiClient.Delete(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task DeleteModel_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.DeleteModel(1);
            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
