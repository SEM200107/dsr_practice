﻿namespace DSR.Api.Test.Tests.Component.Automobile;
using DSR.Api.Controllers.Model.Models;

public partial class ModelIntegrationTest
{
    public static AddModelRequest AddModelRequest(string title)
    {
        return new AddModelRequest()
        {
            Title = title
        };
    }

    public static UpdateModelRequest UpdateModelRequest(string title)
    {
        return new UpdateModelRequest()
        {
            Title = title
        };
    }
}
