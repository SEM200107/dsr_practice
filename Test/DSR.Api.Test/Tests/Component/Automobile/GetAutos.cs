﻿using DSR.Api.Controllers.Auto;
using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class AutomobileIntegrationTest
    {
        [Test]
        public async Task GetAutos_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetAutos();
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var autos_from_api = await response.ReadAsObject<IEnumerable<AutoResponse>>();

            await using var context = await DbContext();
            var autos_from_db = context.Automobiles.AsEnumerable();

            Assert.AreEqual(autos_from_db.Count(), autos_from_api.Count());
        }

        [Test]
        public async Task GetAutos_NegativeParameters_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetAutos(-1, -1);
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var autos_from_api = await response.ReadAsObject<IEnumerable<AutoResponse>>();

            Assert.AreEqual(0, autos_from_api.Count());
        }

        [Test]
        public async Task GetAutos_Unauthorized()
        {
            var url = Urls.GetAutos();
            var response = await apiClient.Get(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task GetAutos_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.GetAutos();
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
