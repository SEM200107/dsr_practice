﻿using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class AutomobileIntegrationTest
    {
        [Test]
        public async Task AddAuto_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddAuto;

            var colorId = await GetExistedColorId();
            var markId = await GetExistedMarkId();
            var request = AddAutoRequest(Generator.ValidNumbers.First(), Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);


            await using var context = await DbContext();
            var newAuto = context.Automobiles.AsEnumerable().OrderByDescending(x => x.Id).FirstOrDefault();
            Assert.IsNotNull(newAuto);

            Assert.AreEqual(request.ColorId, newAuto?.ColorId);
            Assert.AreEqual(request.MarkId, newAuto?.MarkId);
            Assert.AreEqual(request.Number, newAuto?.Number);
            Assert.AreEqual(request.Description, newAuto?.Description);
        }

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidColorIds))]
        public async Task AddAuto_InvalidColor_Authenticated_BadRequest(int colorId)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddAuto;

            var markId = await GetExistedMarkId();
            var request = AddAutoRequest(Generator.ValidNumbers.First(), Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidMarkIds))]
        public async Task AddAuto_InvalidMark_Authenticated_BadRequest(int markId)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddAuto;

            var colorId = await GetExistedColorId();
            var request = AddAutoRequest(Generator.ValidNumbers.First(), Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidNumbers))]
        public async Task AddAuto_InvalidNumber_Authenticated_BadRequest(string number)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddAuto;

            var colorId = await GetExistedColorId();
            var markId = await GetExistedMarkId();
            var request = AddAutoRequest(number, Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidDescriptions))]
        public async Task AddAuto_InvalidDescription_Authenticated_BadRequest(string description)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddAuto;

            var colorId = await GetExistedColorId();
            var markId = await GetExistedMarkId();
            var request = AddAutoRequest(Generator.ValidNumbers.First(), description, colorId, markId);
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [Test]
        public async Task AddAuto_Unauthorized()
        {
            var url = Urls.AddAuto;

            var colorId = await GetExistedColorId();
            var markId = await GetExistedMarkId();
            var request = AddAutoRequest(Generator.ValidNumbers.First(), Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PostJson(url, request, null);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task AddAuto_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.AddAuto;

            var colorId = await GetExistedColorId();
            var markId = await GetExistedMarkId();
            var request = AddAutoRequest(Generator.ValidNumbers.First(), Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
