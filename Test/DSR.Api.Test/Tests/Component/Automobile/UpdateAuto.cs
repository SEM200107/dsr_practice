﻿using DSRNetSchool.Api.Test.Common.Extensions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class AutomobileIntegrationTest
    {
        [Test]
        public async Task UpdateAuto_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var idAuto = await GetExistedAutoId();
            var url = Urls.UpdateAuto(idAuto);

            var colorId = await GetExistedColorId();
            var markId = await GetExistedMarkId();
            var request = UpdateAutoRequest(Generator.ValidNumbers.First(), Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);


            await using var context = await DbContext();
            var newAuto = await context.Automobiles.FirstOrDefaultAsync(x => x.Id.Equals(idAuto));
            Assert.IsNotNull(newAuto);

            Assert.AreEqual(request.ColorId, newAuto?.ColorId);
            Assert.AreEqual(request.MarkId, newAuto?.MarkId);
            Assert.AreEqual(request.Number, newAuto?.Number);
            Assert.AreEqual(request.Description, newAuto?.Description);
        }

        [Test]
        public async Task UpdateAuto_InvalidAutoId_Authenticated_BadRequest()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateAuto(await GetNotExistedAutoId());

            var markId = await GetExistedMarkId();
            var colorId = await GetExistedColorId();
            var request = UpdateAutoRequest(Generator.ValidNumbers.First(), Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidColorIds))]
        public async Task UpdateAuto_InvalidColor_Authenticated_BadRequest(int colorId)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateAuto(await GetExistedAutoId());

            var markId = await GetExistedMarkId();
            var request = UpdateAutoRequest(Generator.ValidNumbers.First(), Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidMarkIds))]
        public async Task UpdateAuto_InvalidMark_Authenticated_BadRequest(int markId)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateAuto(await GetExistedAutoId());

            var colorId = await GetExistedColorId();
            var request = UpdateAutoRequest(Generator.ValidNumbers.First(), Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidNumbers))]
        public async Task UpdateAuto_InvalidNumber_Authenticated_BadRequest(string number)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateAuto(await GetExistedAutoId());

            var colorId = await GetExistedColorId();
            var markId = await GetExistedMarkId();
            var request = UpdateAutoRequest(number, Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidDescriptions))]
        public async Task UpdateAuto_InvalidDescription_Authenticated_BadRequest(string description)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateAuto(await GetExistedAutoId());

            var colorId = await GetExistedColorId();
            var markId = await GetExistedMarkId();
            var request = UpdateAutoRequest(Generator.ValidNumbers.First(), description, colorId, markId);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [Test]
        public async Task UpdateAuto_Unauthorized()
        {
            var url = Urls.UpdateAuto(await GetExistedAutoId());

            var colorId = await GetExistedColorId();
            var markId = await GetExistedMarkId();
            var request = UpdateAutoRequest(Generator.ValidNumbers.First(), Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PutJson(url, request, null);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task UpdateAuto_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.UpdateAuto(await GetExistedAutoId());

            var colorId = await GetExistedColorId();
            var markId = await GetExistedMarkId();
            var request = UpdateAutoRequest(Generator.ValidNumbers.First(), Generator.ValidDescriptions.First(), colorId, markId);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
