﻿namespace DSR.Api.Test.Tests.Component.Automobile;

public partial class AutomobileIntegrationTest
{
    public static class Generator
    {
        public static string[] ValidNumbers =
        {
            new string('1',1),
            new string('1',9)
        };

        public static string[] InvalidNumbers =
        {
            null,
            "",
            new string('1',10)
        };

        public static string[] ValidDescriptions =
        {
            new string('1',1),
            new string('1',1000)
        };

        public static string[] InvalidDescriptions =
        {
            new string('1',1001),
        };

        public static int[] InvalidColorIds =
        {
            0,
            -1,
            int.MaxValue
        };

        public static int[] InvalidMarkIds =
        {
            0,
            -1,
            int.MaxValue
        };        
    }
}
