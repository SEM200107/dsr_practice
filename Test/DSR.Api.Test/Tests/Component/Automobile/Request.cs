﻿namespace DSR.Api.Test.Tests.Component.Automobile;

using DSR.Api.Controllers.Auto;

public partial class AutomobileIntegrationTest
{
    public static AddAutoRequest AddAutoRequest(string number, string description, int colorId, int markId)
    {
        return new AddAutoRequest()
        {
            Number = number,
            Description = description,
            ColorId = colorId,
            MarkId = markId
        };
    }

    public static UpdateAutoRequest UpdateAutoRequest(string number, string description, int colorId, int markId)
    {
        return new UpdateAutoRequest()
        {
            Number = number,
            Description = description,
            ColorId = colorId,
            MarkId = markId
        };
    }
}
