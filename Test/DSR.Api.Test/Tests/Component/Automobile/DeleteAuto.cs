﻿using DSR.Api.Controllers.Auto;
using DSRNetSchool.Api.Test.Common.Extensions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class AutomobileIntegrationTest
    {
        [Test]
        public async Task DeleteAuto_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var idAuto = await GetExistedAutoId();
            var url = Urls.DeleteAuto(idAuto);
            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            await using var context = await DbContext();
            var newAuto = await context.Automobiles.FirstOrDefaultAsync(x => x.Id.Equals(idAuto));
            Assert.IsNull(newAuto);
        }

        [Test]
        public async Task DeleteAuto_NegativeParameters_BadRequest()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.DeleteAuto(await GetNotExistedAutoId());
            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task DeleteAuto_Unauthorized()
        {
            var url = Urls.DeleteAuto(1);
            var response = await apiClient.Delete(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task DeleteAuto_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.DeleteAuto(1);
            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
