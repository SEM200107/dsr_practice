﻿using DSR.Api.Controllers.Auto;
using DSR.Api.Controllers.Comment.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class CommentIntegrationTest
    {
        [Test]
        public async Task GetComments_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetComments(await GetExistedAutoId());
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var comments_from_api = await response.ReadAsObject<IEnumerable<CommentResponse>>();

            await using var context = await DbContext();
            var comments_from_db = context.Comments.AsEnumerable();

            Assert.AreEqual(comments_from_db.Count(), comments_from_api.Count());
        }

        [Test]
        public async Task GetComments_NegativeParameters_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetComments(await GetNotExistedAutoId());
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var comments_from_api = await response.ReadAsObject<IEnumerable<CommentResponse>>();

            Assert.AreEqual(0, comments_from_api.Count());
        }

        [Test]
        public async Task GetComments_Unauthorized()
        {
            var url = Urls.GetComments(1);
            var response = await apiClient.Get(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task GetAutos_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.GetComments(1);
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
