﻿using DSR.Api.Test.Common;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSR.Db.Entities;
using DSR.Shared.Common.Security;
using System;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    [TestFixture]
    public partial class CommentIntegrationTest : ComponentTest
    {
        const string EmailTestUser = "test@test.ru";
        const string PasswordTestUser = "test";

        [SetUp]
        public async Task SetUp()
        {
            await using var context = await DbContext();

            context.Automobiles.RemoveRange(context.Automobiles);
            context.Colors.RemoveRange(context.Colors);
            context.Models.RemoveRange(context.Models);
            context.Marks.RemoveRange(context.Marks);
            context.Comments.RemoveRange(context.Comments);
            context.SaveChanges();

            var model1 = new Db.Entities.Model()
            {
                Title = "Ford"
            };
            context.Models.Add(model1);
            var model2 = new Db.Entities.Model()
            {
                Title = "Kia"
            };
            context.Models.Add(model2);


            var mark1 = new Db.Entities.Mark()
            {
                Model = model1,
                Title = "Focus 3"
            };
            context.Marks.Add(mark1);
            var mark2 = new Db.Entities.Mark()
            {
                Model = model2,
                Title = "Optima"
            };
            context.Marks.Add(mark2);

            var c1 = new Db.Entities.Color()
            {
                Title = "Black"
            };
            context.Colors.Add(c1);
            var c2 = new Db.Entities.Color()
            {
                Title = "White"
            };
            context.Colors.Add(c2);

            var a1 = new Db.Entities.Automobile()
            {
                Number = "A111AA136",
                Description = "Описание 1",
                Color = c1,
                Mark = mark1,
            };
            context.Automobiles.Add(a1);

            var a2 = new Db.Entities.Automobile()
            {
                Number = "A222AA136",
                Description = "Описание 2",
                Color = c2,
                Mark = mark2,
            };
            context.Automobiles.Add(a2);

            context.SaveChanges();
        }

        [TearDown]
        public async override Task TearDown()
        {
            await using var context = await DbContext();
            context.Automobiles.RemoveRange(context.Automobiles);
            context.Colors.RemoveRange(context.Colors);
            context.Models.RemoveRange(context.Models);
            context.Marks.RemoveRange(context.Marks);
            context.Comments.RemoveRange(context.Comments);
            context.SaveChanges();
            await base.TearDown();
        }

        protected static class Urls
        {
            public static string GetComments(int idAuto) => $"/api/v1/comments/{idAuto}";

            public static string GetComment(int id) => $"/api/v1/comments/getcomment/{id}";

            public static string DeleteComment(int id) => $"/api/v1/comments/{id}";

            public static string UpdateComment(int id) => $"/api/v1/comments/{id}";

            public static string AddComment => $"/api/v1/comments";
        }

        public static class Scopes
        {
            public static string ReadAutos => $"offline_access {AppScopes.AutomobilesRead}";

            public static string WriteAutos => $"offline_access {AppScopes.AutomobilesWrite}";

            public static string ReadAndWriteAutos => $"offline_access {AppScopes.AutomobilesRead} {AppScopes.AutomobilesWrite}";

            public static string Empty => "offline_access";
        }

        public async Task<string> AuthenticateUser_ReadAndWriteAutosScope()
        {
            await GetTestUser();
            var tokenResponse = await AuthenticateTestUser(EmailTestUser, PasswordTestUser, Scopes.ReadAndWriteAutos);
            return tokenResponse.AccessToken;
        }

        public async Task<string> AuthenticateUser_EmptyScope()
        {
            await GetTestUser();
            var tokenResponse = await AuthenticateTestUser(EmailTestUser, PasswordTestUser, Scopes.Empty);
            return tokenResponse.AccessToken;
        }

        public async Task<int> GetExistedCommentId()
        {
            await using var context = await DbContext();
            if (context.Comments.Count() == 0)
            {
                Comment comment = new Db.Entities.Comment()
                {
                    Content = "Test",
                    UserId = await GetExistedUserId(),
                    AutomobileId = await GetExistedAutoId()
                };
                context.Comments.Add(comment);
                context.SaveChanges();
            }

            await using var context1 = await DbContext();
            var comment1 = context1.Comments.AsEnumerable().First();
            return comment1.Id;
        }

        public async Task<Guid> GetExistedUserId()
        {
           return (await userManager.FindByEmailAsync(EmailTestUser)).Id;
        }

        public async Task<int> GetExistedAutoId()
        {
            await using var context = await DbContext();
            if (context.Automobiles.Count() == 0)
            {
                Db.Entities.Automobile auto = new Db.Entities.Automobile()
                {
                    Number = "T111ST111",
                    Description = "Description",
                    Color = new Db.Entities.Color()
                    {
                        Title = "Test"
                    },
                    Mark = new Mark()
                    {
                        Title = "Test",
                        Model = new Db.Entities.Model()
                        {
                            Title = "Test"
                        }
                    }

                };
                context.Automobiles.Add(auto);
                context.SaveChanges();
            }

            await using var context1 = await DbContext();
            var auto1 = context1.Automobiles.AsEnumerable().First();
            return auto1.Id;
        }

        public async Task<int> GetNotExistedAutoId()
        {
            await using var context = await DbContext();
            var maxExistedAutoId = context.Automobiles.Max(x => x.Id);

            return maxExistedAutoId + 1;
        }        
    }
}
