﻿namespace DSR.Api.Test.Tests.Component.Automobile;

public partial class CommentIntegrationTest
{
    public static class Generator
    {
        public static string[] ValidComments =
        {
            new string('1',1),
            new string('1',500)
        };

        public static string[] InvalidComments =
        {
            null,
            "",
            new string('1',501)
        };              
    }
}
