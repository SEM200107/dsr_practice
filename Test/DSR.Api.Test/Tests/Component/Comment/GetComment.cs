﻿using DSR.Api.Controllers.Comment.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class CommentIntegrationTest
    {
        [Test]
        public async Task GetComment_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetComment(await GetExistedCommentId());
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var comments_from_api = await response.ReadAsObject<CommentResponse>();

            Assert.AreEqual(await GetExistedCommentId(), comments_from_api.Id);
        }

        [Test]
        public async Task GetComment_NegativeParameters_NoContent()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetComment(int.MaxValue);
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
        }

        [Test]
        public async Task GetComment_Unauthorized()
        {
            var url = Urls.GetComment(1);
            var response = await apiClient.Get(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task GetComment_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.GetComment(1);
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
