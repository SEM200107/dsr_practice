﻿namespace DSR.Api.Test.Tests.Component.Automobile;
using DSR.Api.Controllers.Comment.Models;
using System;

public partial class CommentIntegrationTest
{
    public static AddCommentRequest AddCommentRequest(string content, Guid userId, int autoId)
    {
        return new AddCommentRequest()
        {
           Content = content,
           UserId = userId,
           AutomobileId = autoId
        };
    }

    public static UpdateCommentRequest UpdateCommentRequest(string content, Guid userId, int autoId)
    {
        return new UpdateCommentRequest()
        {
            Content = content,
            UserId = userId,
            AutomobileId = autoId
        };
    }
}
