﻿using DSR.Api.Test.Common;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSR.Db.Entities;
using DSR.Shared.Common.Security;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    [TestFixture]
    public partial class ColorIntegrationTest : ComponentTest
    {
        const string EmailTestUser = "test@test.ru";
        const string PasswordTestUser = "test";

        [SetUp]
        public async Task SetUp()
        {
            await using var context = await DbContext();

            context.Automobiles.RemoveRange(context.Automobiles);
            context.Colors.RemoveRange(context.Colors);
            context.Models.RemoveRange(context.Models);
            context.Marks.RemoveRange(context.Marks);
            context.Comments.RemoveRange(context.Comments);
            context.SaveChanges();

            var model1 = new Db.Entities.Model()
            {
                Title = "Ford"
            };
            context.Models.Add(model1);
            var model2 = new Db.Entities.Model()
            {
                Title = "Kia"
            };
            context.Models.Add(model2);


            var mark1 = new Db.Entities.Mark()
            {
                Model = model1,
                Title = "Focus 3"
            };
            context.Marks.Add(mark1);
            var mark2 = new Db.Entities.Mark()
            {
                Model = model2,
                Title = "Optima"
            };
            context.Marks.Add(mark2);

            var c1 = new Db.Entities.Color()
            {
                Title = "Black"
            };
            context.Colors.Add(c1);
            var c2 = new Db.Entities.Color()
            {
                Title = "White"
            };
            context.Colors.Add(c2);

            var a1 = new Db.Entities.Automobile()
            {
                Number = "A111AA136",
                Description = "Описание 1",
                Color = c1,
                Mark = mark1,
            };
            context.Automobiles.Add(a1);

            var a2 = new Db.Entities.Automobile()
            {
                Number = "A222AA136",
                Description = "Описание 2",
                Color = c2,
                Mark = mark2,
            };
            context.Automobiles.Add(a2);

            context.SaveChanges();
        }

        [TearDown]
        public async override Task TearDown()
        {
            await using var context = await DbContext();
            context.Automobiles.RemoveRange(context.Automobiles);
            context.Colors.RemoveRange(context.Colors);
            context.Models.RemoveRange(context.Models);
            context.Marks.RemoveRange(context.Marks);
            context.Comments.RemoveRange(context.Comments);
            context.SaveChanges();
            await base.TearDown();
        }

        protected static class Urls
        {
            public static string GetColors() => $"/api/v1/colors";

            public static string GetColor(int id) => $"/api/v1/colors/{id}";

            public static string DeleteColor(int id) => $"/api/v1/colors/{id}";

            public static string UpdateColor(int id) => $"/api/v1/colors/{id}";

            public static string AddColor => $"/api/v1/colors";
        }

        public static class Scopes
        {
            public static string ReadAutos => $"offline_access {AppScopes.AutomobilesRead}";

            public static string WriteAutos => $"offline_access {AppScopes.AutomobilesWrite}";

            public static string ReadAndWriteAutos => $"offline_access {AppScopes.AutomobilesRead} {AppScopes.AutomobilesWrite}";

            public static string Empty => "offline_access";
        }

        public async Task<string> AuthenticateUser_ReadAndWriteAutosScope()
        {
            await GetTestUser();
            var tokenResponse = await AuthenticateTestUser(EmailTestUser, PasswordTestUser, Scopes.ReadAndWriteAutos);
            return tokenResponse.AccessToken;
        }

        public async Task<string> AuthenticateUser_EmptyScope()
        {
            await GetTestUser();
            var tokenResponse = await AuthenticateTestUser(EmailTestUser, PasswordTestUser, Scopes.Empty);
            return tokenResponse.AccessToken;
        }

        public async Task<int> GetExistedColorId()
        {
            await using var context = await DbContext();
            if (context.Colors.Count() == 0)
            {
                Color color = new Color()
                {
                    Title = "Test"
                };
                context.Colors.Add(color);
                context.SaveChanges();
            }

            await using var context1 = await DbContext();
            var color1 = context1.Colors.AsEnumerable().First();
            return color1.Id;
        }

        public async Task<int> GetNotExistedColorId()
        {
            await using var context = await DbContext();
            var maxExistedColorId = context.Colors.Max(x => x.Id);

            return maxExistedColorId + 1;
        }
    }
}
