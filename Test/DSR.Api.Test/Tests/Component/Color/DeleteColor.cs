﻿using DSR.Api.Controllers.Mark.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class ColorIntegrationTest
    {
        [Test]
        public async Task DeleteColor_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var idColor = await GetExistedColorId();
            var url = Urls.DeleteColor(idColor);
            await using var context = await DbContext();

            var auto = await context.Automobiles.FirstOrDefaultAsync(x => x.ColorId == idColor);
            context.Automobiles.Remove(auto);
            context.SaveChanges();

            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            
            var newColor = await context.Colors.FirstOrDefaultAsync(x => x.Id.Equals(idColor));
            Assert.IsNull(newColor);
        }

        [Test]
        public async Task DeleteColor_NegativeParameters_BadRequest()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.DeleteColor(await GetNotExistedColorId());
            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task DeleteColor_Unauthorized()
        {
            var url = Urls.DeleteColor(1);
            var response = await apiClient.Delete(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task DeleteColor_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.DeleteColor(1);
            var response = await apiClient.Delete(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
