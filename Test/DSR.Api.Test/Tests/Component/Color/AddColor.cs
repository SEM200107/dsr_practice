﻿using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class ColorIntegrationTest
    {
        [Test]
        public async Task AddColor_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddColor;

            var request = AddColorRequest(Generator.ValidTitles.First());
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);


            await using var context = await DbContext();
            var newColor = context.Colors.AsEnumerable().OrderByDescending(x => x.Id).FirstOrDefault();

            Assert.IsNotNull(newColor);
            Assert.AreEqual(request.Title, newColor?.Title);
        }        

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidTitles))]
        public async Task AddColor_InvalidTitle_Authenticated_BadRequest(string title)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.AddColor;

            var request = AddColorRequest(title);
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [Test]
        public async Task AddColor_Unauthorized()
        {
            var url = Urls.AddColor;

            var request = AddColorRequest(Generator.ValidTitles.First());
            var response = await apiClient.PostJson(url, request, null);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task AddColor_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.AddColor;

            var request = AddColorRequest(Generator.ValidTitles.First());
            var response = await apiClient.PostJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
