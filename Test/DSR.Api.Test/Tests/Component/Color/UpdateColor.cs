﻿using DSRNetSchool.Api.Test.Common.Extensions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class ColorIntegrationTest
    {
        [Test]
        public async Task UpdateColor_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var idColor = await GetExistedColorId();
            var url = Urls.UpdateColor(idColor);

            var request = UpdateColorRequest(Generator.ValidTitles.First());
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);


            await using var context = await DbContext();
            var newColor = await context.Colors.FirstOrDefaultAsync(x => x.Id.Equals(idColor));

            Assert.IsNotNull(newColor);
            Assert.AreEqual(request.Title, newColor?.Title);
        }

        [Test]
        public async Task UpdateColor_InvalidColorId_Authenticated_BadRequest()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateColor(await GetNotExistedColorId());

            var request = UpdateColorRequest(Generator.ValidTitles.First());
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }        

        [Test]
        [TestCaseSource(typeof(Generator), nameof(Generator.InvalidTitles))]
        public async Task UpdateColor_InvalidTitle_Authenticated_BadRequest(string title)
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.UpdateColor(await GetExistedColorId());

            var request = UpdateColorRequest(title);
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async Task UpdateColor_Unauthorized()
        {
            var url = Urls.UpdateColor(await GetExistedColorId());

            var request = UpdateColorRequest(Generator.ValidTitles.First());
            var response = await apiClient.PutJson(url, request, null);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task UpdateColor_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.UpdateColor(await GetExistedColorId());

            var request = AddColorRequest(Generator.ValidTitles.First());
            var response = await apiClient.PutJson(url, request, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
