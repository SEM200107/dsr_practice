﻿namespace DSR.Api.Test.Tests.Component.Automobile;
using DSR.Api.Controllers.Color.Models;

public partial class ColorIntegrationTest
{
    public static AddColorRequest AddColorRequest(string title)
    {
        return new AddColorRequest()
        {
            Title = title
        };
    }

    public static UpdateColorRequest UpdateColorRequest(string title)
    {
        return new UpdateColorRequest()
        {
            Title = title
        };
    }
}
