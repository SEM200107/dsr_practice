﻿using DSR.Api.Controllers.Color.Models;
using DSRNetSchool.Api.Test.Common.Extensions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DSR.Api.Test.Tests.Component.Automobile
{
    public partial class ColorIntegrationTest
    {
        [Test]
        public async Task GetColors_ValidParameters_Authenticated_OkResponse()
        {
            var accessToken = await AuthenticateUser_ReadAndWriteAutosScope();
            var url = Urls.GetColors();
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var colors_from_api = await response.ReadAsObject<IEnumerable<ColorResponse>>();

            await using var context = await DbContext();
            var colors_from_db = context.Marks.AsEnumerable();

            Assert.AreEqual(colors_from_db.Count(), colors_from_api.Count());
        }

        [Test]
        public async Task GetColors_Unauthorized()
        {
            var url = Urls.GetColors();
            var response = await apiClient.Get(url);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public async Task GetColors_EmptyScope_Forbidden()
        {
            var accessToken = await AuthenticateUser_EmptyScope();
            var url = Urls.GetColors();
            var response = await apiClient.Get(url, accessToken);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }
    }
}
