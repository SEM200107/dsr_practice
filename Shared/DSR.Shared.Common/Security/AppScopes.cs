﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.Shared.Common.Security
{
    public static class AppScopes
    {
        public const string AutomobilesRead = "automobiles_read";
        public const string AutomobilesWrite = "automobiles_write";
    }
}
