﻿using DSR.Db.Entities;
using DSR.Db.Entities.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.Db.Context.Context
{
    public class MainDbContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public DbSet<Model> Models { get; set; }
        public DbSet<Mark> Marks { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Automobile> Automobiles { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public MainDbContext(DbContextOptions<MainDbContext> options) : base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().ToTable("users");
            modelBuilder.Entity<IdentityRole<Guid>>().ToTable("user_roles");
            modelBuilder.Entity<IdentityUserToken<Guid>>().ToTable("user_tokens");
            modelBuilder.Entity<IdentityUserRole<Guid>>().ToTable("user_role_owners");
            modelBuilder.Entity<IdentityRoleClaim<Guid>>().ToTable("user_role_claims");
            modelBuilder.Entity<IdentityUserLogin<Guid>>().ToTable("user_logins");
            modelBuilder.Entity<IdentityUserClaim<Guid>>().ToTable("user_claims");

            modelBuilder.Entity<Model>().ToTable("models");
            modelBuilder.Entity<Model>().HasIndex(x => x.Title).IsUnique();

            modelBuilder.Entity<Mark>().ToTable("marks");
            modelBuilder.Entity<Mark>().HasIndex(x => x.Title).IsUnique();
            modelBuilder.Entity<Mark>().HasOne(x => x.Model).WithMany(x => x.Marks).HasForeignKey(x => x.ModelId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Color>().ToTable("colors");
            modelBuilder.Entity<Color>().HasIndex(x => x.Title).IsUnique();

            modelBuilder.Entity<Automobile>().ToTable("automobiles");
            modelBuilder.Entity<Automobile>().HasIndex(x => x.Number).IsUnique();
            modelBuilder.Entity<Automobile>().HasOne(x => x.Mark).WithMany(x => x.Automobiles).HasForeignKey(x => x.MarkId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Automobile>().HasOne(x => x.Color).WithMany(x => x.Automobiles).HasForeignKey(x => x.ColorId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Comment>().ToTable("comments");
            modelBuilder.Entity<Comment>().HasOne(x => x.User).WithMany(x => x.Comments).HasForeignKey(x => x.UserId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Comment>().HasOne(x => x.Automobile).WithMany(x => x.Comments).HasForeignKey(x => x.AutomobileId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
