﻿using DSR.Db.Context.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSR.Db.Context.Setup
{
    public static class DbSeed
    {
        public static void Execute(IServiceProvider serviceProvider)
        {
            using var scope = serviceProvider.GetService<IServiceScopeFactory>()?.CreateScope();
            ArgumentNullException.ThrowIfNull(scope);

            var factory = scope.ServiceProvider.GetRequiredService<IDbContextFactory<MainDbContext>>();
            using var context = factory.CreateDbContext();

            AddAutos(context);

        }
        private static void AddAutos(MainDbContext context)
        {
            if (context.Automobiles.Any() || context.Models.Any() || context.Marks.Any() || context.Colors.Any())
                return;

            var model1 = new Entities.Model()
            {
                Title = "Ford"
            };
            context.Models.Add(model1);
            var model2 = new Entities.Model()
            {
                Title = "Kia"
            };
            context.Models.Add(model2);


            var mark1 = new Entities.Mark()
            {
                Model = model1,
                Title = "Focus 3"
            };
            context.Marks.Add(mark1);
            var mark2 = new Entities.Mark()
            {
                Model = model2,
                Title = "Optima"
            };
            context.Marks.Add(mark2);

            var c1 = new Entities.Color()
            {
                Title = "Black"
            };
            context.Colors.Add(c1);
            var c2 = new Entities.Color()
            {
                Title = "White"
            };
            context.Colors.Add(c2);

            var a1 = new Entities.Automobile()
            {
                Number = "A111AA136",
                Description = "Описание 1",
                Color = c1,
                Mark = mark1,
            };
            context.Automobiles.Add(a1);

            var a2 = new Entities.Automobile()
            {
                Number = "A222AA136",
                Description = "Описание 2",
                Color = c2,
                Mark = mark2,
            };
            context.Automobiles.Add(a2);

            context.SaveChanges();
        }
    }
}
