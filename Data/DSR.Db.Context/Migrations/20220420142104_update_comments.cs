﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DSR.Db.Context.Migrations
{
    public partial class update_comments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_comments_automobiles_IdAutomobile",
                table: "comments");

            migrationBuilder.DropForeignKey(
                name: "FK_comments_users_IdUser",
                table: "comments");

            migrationBuilder.RenameColumn(
                name: "IdUser",
                table: "comments",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "IdAutomobile",
                table: "comments",
                newName: "AutomobileId");

            migrationBuilder.RenameIndex(
                name: "IX_comments_IdUser",
                table: "comments",
                newName: "IX_comments_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_comments_IdAutomobile",
                table: "comments",
                newName: "IX_comments_AutomobileId");

            migrationBuilder.AddForeignKey(
                name: "FK_comments_automobiles_AutomobileId",
                table: "comments",
                column: "AutomobileId",
                principalTable: "automobiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_comments_users_UserId",
                table: "comments",
                column: "UserId",
                principalTable: "users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_comments_automobiles_AutomobileId",
                table: "comments");

            migrationBuilder.DropForeignKey(
                name: "FK_comments_users_UserId",
                table: "comments");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "comments",
                newName: "IdUser");

            migrationBuilder.RenameColumn(
                name: "AutomobileId",
                table: "comments",
                newName: "IdAutomobile");

            migrationBuilder.RenameIndex(
                name: "IX_comments_UserId",
                table: "comments",
                newName: "IX_comments_IdUser");

            migrationBuilder.RenameIndex(
                name: "IX_comments_AutomobileId",
                table: "comments",
                newName: "IX_comments_IdAutomobile");

            migrationBuilder.AddForeignKey(
                name: "FK_comments_automobiles_IdAutomobile",
                table: "comments",
                column: "IdAutomobile",
                principalTable: "automobiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_comments_users_IdUser",
                table: "comments",
                column: "IdUser",
                principalTable: "users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
