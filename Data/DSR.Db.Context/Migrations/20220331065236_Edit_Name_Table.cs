﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DSR.Db.Context.Migrations
{
    public partial class Edit_Name_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_automobiles_calors_ColorId",
                table: "automobiles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_calors",
                table: "calors");

            migrationBuilder.RenameTable(
                name: "calors",
                newName: "colors");

            migrationBuilder.RenameIndex(
                name: "IX_calors_Uid",
                table: "colors",
                newName: "IX_colors_Uid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_colors",
                table: "colors",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_automobiles_colors_ColorId",
                table: "automobiles",
                column: "ColorId",
                principalTable: "colors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_automobiles_colors_ColorId",
                table: "automobiles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_colors",
                table: "colors");

            migrationBuilder.RenameTable(
                name: "colors",
                newName: "calors");

            migrationBuilder.RenameIndex(
                name: "IX_colors_Uid",
                table: "calors",
                newName: "IX_calors_Uid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_calors",
                table: "calors",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_automobiles_calors_ColorId",
                table: "automobiles",
                column: "ColorId",
                principalTable: "calors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
