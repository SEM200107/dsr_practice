﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DSR.Db.Context.Migrations
{
    public partial class IsUnique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_models_Title",
                table: "models",
                column: "Title",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_marks_Title",
                table: "marks",
                column: "Title",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_colors_Title",
                table: "colors",
                column: "Title",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_automobiles_Number",
                table: "automobiles",
                column: "Number",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_models_Title",
                table: "models");

            migrationBuilder.DropIndex(
                name: "IX_marks_Title",
                table: "marks");

            migrationBuilder.DropIndex(
                name: "IX_colors_Title",
                table: "colors");

            migrationBuilder.DropIndex(
                name: "IX_automobiles_Number",
                table: "automobiles");
        }
    }
}
