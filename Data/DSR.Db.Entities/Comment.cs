﻿using DSR.Db.Entities.Common;
using DSR.Db.Entities.User;

namespace DSR.Db.Entities
{
    public class Comment : BaseEntity
    {
        public string Content { get; set; }

        public Guid UserId { get; set; }
        public virtual DSR.Db.Entities.User.User User { get; set; }

        public int AutomobileId { get; set; }
        public virtual Automobile Automobile { get; set; }

    }
}
