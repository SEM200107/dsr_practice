﻿using DSR.Db.Entities.Common;

namespace DSR.Db.Entities
{
    public class Automobile : BaseEntity
    {
        public string Number { get; set; }
        public string Description { get; set; }

        public int ColorId { get; set; }
        public virtual Color Color { get; set; } 

        public int MarkId { get; set; }
        public virtual Mark Mark { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

    }
}
