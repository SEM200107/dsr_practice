﻿using DSR.Db.Entities.Common;

namespace DSR.Db.Entities
{
    public class Model : BaseEntity
    {
        public string Title { get; set; }
        public virtual ICollection<Mark> Marks { get; set; }
    }
}
