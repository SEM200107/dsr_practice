﻿using Microsoft.AspNetCore.Identity;

namespace DSR.Db.Entities.User
{
    public class User : IdentityUser<Guid>
    {
        public string FullName { get; set; }
        public UserStatus Status { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
