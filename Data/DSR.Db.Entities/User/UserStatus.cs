﻿namespace DSR.Db.Entities.User
{
    public enum UserStatus
    {
        Active = 0,
        Blocked = 1
    }
}
