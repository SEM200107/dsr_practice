﻿using DSR.Db.Entities.Common;

namespace DSR.Db.Entities
{
    public class Mark : BaseEntity
    {
        public string Title { get; set; }
        public int ModelId { get; set; }
        public virtual Model Model { get; set; }
        public virtual ICollection<Automobile> Automobiles { get; set; }
    }
}
