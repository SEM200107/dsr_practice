﻿using DSR.Db.Entities.Common;

namespace DSR.Db.Entities
{
    public class Color : BaseEntity
    {
        public string Title { get; set; }
        public virtual ICollection<Automobile> Automobiles { get; set; }
    }
}
